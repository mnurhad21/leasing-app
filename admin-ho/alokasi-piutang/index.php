<?php
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
 session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

	$cashflow = $log->getCashflow();
	$leasing = $log->getLeasing();
	$nasabah = $log->getNasabah();
	$cashflowId = $log->getCashflowId();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/picker_date.js"></script>

</head>

<body>


	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/adminoh/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Alokasi</span> - Piutang</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">piutang</a></li>
							<li class="active">Alokasi Piutang</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					
					<form action="" method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Alokasi Piutang</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">

								<div class="form-group">
									<label class="col-lg-2 control-label">Transaksi:</label>
									<div class="col-lg-9">
										<select class="select" id="transaksiId" onchange="cek_database()" name="transaksiId">
											<optgroup label="Id Cashflow">
												<option>-- Pilih --</option>
												<?php if(is_array($cashflowId)) {
												 foreach($cashflowId as $ro) {
												 ?>
												<option value="<?php echo $ro['TC_TRANS_ID']; ?>"><?php echo $ro['TC_TRANS_ID']; ?></option>
												<?php } } ?>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Terima dari:</label>
									<div class="col-lg-9">
										<input type="text" name="terimaDari" id="terimaDari" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Terima Dari">
									</div>
								</div>

								<input type="hidden" name="terimaDariId" id="terimaDariId">

								<div class="form-group">
									<label class="col-lg-2 control-label">Nominal:</label>
									<div class="col-lg-9">
										<div class="input-group">
											<span class="input-group-addon">Rp.</span>
											<input type="text" name="nominal" id="nominal" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nominal">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nama Customer:</label>
									<div class="col-lg-9">
										<select class="select" name="idCustomer" id="idCustomer" onchange="cek_database()" required>
											<optgroup label="Customer ID">
												<option value="">Pilih Customer</option>
												<?php foreach($nasabah as $nsbh){ ?>
												<option value="<?php echo $nsbh['TN_CUSTID']; ?>"><?php echo $nsbh['TN_NAMA']; ?></option>
												<?php  } ?>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar"></i></span>
											<input type="text" id="from" class="form-control" name="tanggalAwal" placeholder="Pilih Tanggal Awal&hellip;">
										</div>
									</div>
									<div class="col-lg-6">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
												<input type="text" id="to" class="form-control" name="tanggalAkhir" placeholder="Pilih Tanggal Akhir&hellip;">
											</div>
									</div>
								</div>

								<div class="text-right">
									<button name="submit" class="btn btn-primary">Cari Data<i class="icon-search4 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Scrollable datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Alokasi Piutang</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>
						<?php if(isset($_POST['submit'])){
							$alokasi['idCustomer'] = $_POST['idCustomer'];
						    // $alokasi['namaCust'] = $_POST['namaCust'];
						    $alokasi['terimaDari'] = $_POST['terimaDari'];
						    $alokasi['terimaDariId'] = $_POST['terimaDariId'];
						    $alokasi['transaksiId'] = $_POST['transaksiId'];
						    $alokasi['tanggalAwal'] = $_POST['tanggalAwal'];
						    $alokasi['tanggalAkhir'] = $_POST['tanggalAkhir'];
						    $ctrl = $log->getAlokasi($alokasi);
						 ?>
						<table id="example" class="table datatable" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Tanggal</th>
									<th class="text-center">No ID</th>
									<!-- <th>Customer ID</th> -->
									<th>Nama</th>
									<th>No Telp</th>
									<th class="text-center">Alamat</th>
									<th>Type Penjualan</th>
									<!-- <th class="text-center">Leasing ID</th> -->
									<th class="text-center">Nama Leasing</th>
									<th>Unit ID</th>
									<th>Type</th>
									<th>Warna</th>
									<th>No Mesin</th>
									<th>No Rangka</th>
									<th>Harga Unit</th>
									<th>DP</th>
									<th>Piutang</th>
									<th>OD</th>
									<th>Keterangan</th>
									<th>Status</th>
									<th>Piutang</th>
									<th style="text-align: center;">Alokasi Piutang</th>
								</tr>
							</thead>
							<tbody>
								<?php
								 $no = 1;
								 if(is_array($ctrl) || is_object($ctrl)) {
								 foreach($ctrl as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $log->TanggalIndo($row["TP_TANGGAL"]); ?></td>
									<td><?= $row["TP_NOID"]; ?></td>
									<td><?= $row["TP_NAMACUST"]; ?></td>
									<td><?= $row["TP_NOHP"]; ?></td>
									<td><?= $row["TP_ALAMAT"]; ?></td>
									<td><?= $row["TP_TYPE_PENJUALAN"]; ?></td>
									<td class="text-center"><?= $row["TL_PRSH_NAMA"]; ?></td>
									<td><?= $row["TP_UNIT_ID"]; ?></td>
									<td><?= $row["TP_TYPE_NAMA"]; ?></td>
									<td><?= $row["TP_WARNA"]; ?></td>
									<td><?= $row["TP_NO_MESIN"]; ?></td>
									<td><?= $row["TP_NO_RANGKA"]; ?></td>
									<td>Rp. <?= number_format($row["TP_HARGA"],2,',','.'); ?></td>
									<td>Rp. <?= number_format($row["TP_DP"],2,',','.'); ?></td>
									<td>Rp. <?= number_format($row["TP_HASIL"],2,',','.'); ?></td>
									<td><?php $booking    =new DateTime($row['TP_TANGGAL']);
				                        $today        =new DateTime();
				                        $diff = $today->diff($booking);
				                        echo $diff->d; echo " Hari";
				                    ?></td>
				                    <td><?= $row["TP_KETERANGAN"]; ?></td>
									<td>
										<select class="select" name="status" disabled>
											<optgroup label="Pilih Status">
												<option value="SURVEY"<?php if($row["TP_STATUS"] == "SURVEY"){echo "selected";} ?>>Proses Survey</option>
												<option value="ON_KREDIT" <?php if($row["TP_STATUS"] == "ON_KREDIT"){echo "selected";} ?>>Pemenuhan Syarat Kredit</option>
												<option value="ON_APPROVAL" <?php if($row["TP_STATUS"] == "ON_APPROVAL"){echo "selected";} ?>>Persetujuan Kredit</option>
												<option value="ON_VERIFIK" <?php if($row["TP_STATUS"] == "ON_VERIFIK"){echo "selected";} ?>>Verifikasi Konsumen</option>
												<option value="ON_ORDER" <?php if($row["TP_STATUS"] == "ON_ORDER"){echo "selected";} ?>>Pencairan Order</option>
											</optgroup>
										</select>
									</td>
									<td>Rp.<?= number_format($row["TP_ANGSURAN"],2,',','.')." X ".$row["TP_TENOR"]; ?></td>
									<td>
										<form method="POST" action="status-alokasi">
											<input type="hidden" name="transaksiId" value="<?php echo $_POST["transaksiId"]; ?>">
											<input type="hidden" name="penjualanId" value="<?php echo $row['TP_LEASING_ID']; ?>">
											<button name="submitValue" class="btn btn-primary">Alokasi <i class="icon-paperplane"></i></button>
										</form>
									</td>
								</tr>
								<?php } }?>
							</tbody>
						</table>
						<?php } ?>
					</div>
					<!-- /scrollable datatable -->
					 


					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<style type="text/css">
		.dropdown {
		  position: sticky;
		  top:50%;
		  transform: translateY(-50%);
		}

		a {
		  color: #fff;
		}

		.dropdown dd,
		.dropdown dt {
		  margin: 0px;
		  padding: 0px;
		}

		.dropdown ul {
		  margin: -1px 0 0 0;
		}

		.dropdown dd {
		  position: sticky;
		}

		.dropdown a,
		.dropdown a:visited {
		  color: #fff;
		  text-decoration: none;
		  outline: none;
		  font-size: 12px;
		}

		.dropdown dt a {
		  background-color: #214256;
		  display: block;
		  padding: 8px 20px 5px 10px;
		  min-height: 25px;
		  line-height: 24px;
		  overflow: hidden;
		  border: 0;
		  width: 272px;
		}

		.dropdown dt a span,
		.multiSel span {
		  cursor: pointer;
		  display: inline-block;
		  padding: 0 3px 2px 0;
		}

		.dropdown dd ul {
		  background-color: #4F6877;
		  border: 0;
		  color: #fff;
		  display: none;
		  left: 0px;
		  padding: 2px 15px 2px 5px;
		  position: absolute;
		  top: 2px;
		  width: 280px;
		  list-style: none;
		  height: 100px;
		  overflow: auto;
		}

		.dropdown span.value {
		  display: none;
		}

		.dropdown dd ul li a {
		  padding: 5px;
		  display: block;
		}

		.dropdown dd ul li a:hover {
		  background-color: #fff;
		}

		button {
		  background-color: #6BBE92;
		  width: 302px;
		  border: 0;
		  padding: 10px 0;
		  margin: 5px 0;
		  text-align: center;
		  color: #fff;
		  font-weight: bold;
		}
	</style>
	<script type="text/javascript">
		$(".dropdown dt a").on('click', function() {
		  $(".dropdown dd ul").slideToggle('fast');
		});

		$(".dropdown dd ul li a").on('click', function() {
		  $(".dropdown dd ul").hide();
		});

		function getSelectedValue(id) {
		  return $("#" + id).find("dt a span.value").html();
		}

		$(document).bind('click', function(e) {
		  var $clicked = $(e.target);
		  if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
		});
	</script>
	<!-- /page container -->
	<script type="text/javascript">
	    jQuery(document).ready(function($){
	        $('.status-link').on('click',function(){
	            var getLink = $(this).attr('href');
	            var id = $("#id").val();
	            swal({
	                    title: 'Apakah Anda Yakin merubah status order?',
	                    text: 'Ubah Status Order',
	                    type: 'warning',
	                    html: true,
	                    confirmButtonColor: '#d9534f',
	                    showCancelButton: true,
	                    },function(){
	                        //createOverlay("Mohon Tunggu...");
	                         $.ajax({  
						        type  : "POST",
						        url   : "alokasi-status",
						        data  : "id=" + id,
						        success : function(result) { 
						          gOverlay.hide();
						          var data = JSON.parse(result);

						          if(data["STATUS"] == "SUCCESS") {            
						            setTimeout(function(){ 
						              window.location = "alokasi-piutang";
						            }, 300);              
						          }
						          else {
						            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
						            //toastr.error(data["MESSAGE"]);
						            swal({
						              title: "GAGAL",
						              text: data["MESSAGE"],
						              type: "error",
						              showCancelButton: false,
						              confirmButtonColor: "#DD6B55",
						              confirmButtonText: "OK",
						              closeOnConfirm: false,
						              html: true
						            },
						            function(){
						              setTimeout(function(){ 
						                window.location = "alokasi-piutang";
						              }, 500);              
						            });
						          }
						        },
						        error : function(error) {   
						          gOverlay.hide();
						          alert("Gangguan pada server/jaringan\r\n" + error);
						        }
						      }); 
	                    	window.location.href = getLink
	                });
	            return false;
	        });
	    });
    </script>
	<script type="text/javascript">
     $(function(){
        $("#to").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to").datepicker( "option", "minDate", minValue );
        })
    });
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#example').DataTable( {
		        "scrollY": 200,
		        "scrollX": true
		    } );
		} );
	</script>
    <script type="text/javascript">
    	function cek_database(){
        var custId = $("#transaksiId").val();
       // console.log(custId);
        $.ajax({
            url: 'alokasi-fix',
            data:"transaksiId="+custId,
        }).success(function (data) {
            var json = data,
            obj = JSON.parse(json);
            $('#terimaDari').val(obj.leasing);
            $('#terimaDariId').val(obj.leasingId);
            $('#nominal').val(obj.nominal);
        });
    }
    </script>
   
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

