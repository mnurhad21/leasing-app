<div class="navbar navbar-default header-highlight" style="background-color: #9c1010;">
		<div class="navbar-header">
			<?php if($_SESSION["U_GROUP_RULE"] == "GR_ADMIN_DEALER") { ?>
			<a class="navbar-brand" href="dashboard-adm-dealer" style="color: #fff"><b>LEASING APP</b></a>
			<?php } elseif($_SESSION["U_GROUP_RULE"] == "GR_ADMIN_HEAD") { ?>
			<a class="navbar-brand" href="dashboard-adm-oh" style="color: #fff"><b>LEASING APP</b></a>
			<?php } else { ?>
			<a href="dashboard-adm-leasing" class="navbar-brand" style="color: #fff">LEASING APP</a>
			<?php } ?>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				
				<li class="dropdown">
					<p class="navbar-text"><span class="label bg-success"><?php if($_SESSION["U_ID"]) {echo "Online";} ?></span></p>
					
					<div class="dropdown-menu dropdown-content width-350">
						<div class="dropdown-content-heading">
							Messages
							<ul class="icons-list">
								<li><a href="#"><i class="icon-compose"></i></a></li>
							</ul>
						</div>
					</div>
				</li>

				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="assets/images/placeholder.jpg" alt="">
						<span style="color: #fff"><?php echo $_SESSION["U_FULLNAME"]; ?></span>
						 <i class="caret" style="color: #fff"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="my-profile"><i class="icon-user-plus"></i> My profile</a></li>
						<li class="divider"></li>
						<li><a href="setting-account"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="logout"  onclick="return confirm('Apakah kamu yakin mau keluar?');"><i class="icon-switch2"></i> Logout</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>