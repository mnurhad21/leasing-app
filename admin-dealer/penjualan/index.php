<?php session_start();
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();
	if(isset($_POST['submit'])){
		$jual['transaksiID'] = $_POST['transaksiID'];
	    $jual['tanggal'] = $_POST['tanggal'];
	    $jual['idCustomer'] = $_POST['idCustomer'];
	    $jual['nama'] = $_POST['nama'];
	    $jual['noTelp'] = $_POST['noTelp'];
	    $jual['alamat'] = $_POST['alamat'];
	    $jual['typePenjualan'] = $_POST['typePenjualan'];
	    $jual['leasingId'] = $_POST['leasingId'];
	    $jual['uangMuka'] = $_POST['uangMuka'];
	    $jual['tenor'] = $_POST['tenor'];
	    $jual['angsuran'] = $_POST['angsuran'];
	    $jual['unitId'] = $_POST['unitId'];
	    $jual['unitNama'] = $_POST['unitNama'];
	    $jual['unitWarna'] = $_POST['unitWarna'];
	    $jual['unitRangka'] = $_POST['unitRangka'];
	    $jual['unitMesin'] = $_POST['unitMesin'];
	    $jual['unitHarga'] = $_POST['unitHarga'];
	    $ctrl = $log->tambahPenjualan($jual);
	}

	$unit = $log->getUnitStatus();
	$leasing = $log->getLeasing();
	$nasabah = $log->getNasabah();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/picker_date.js"></script>

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/admindealer/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Transaksi</span> - Penjualan</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="form_inputs_grid.html">Transaksi</a></li>
							<li class="active">Penjualan</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Multiple row inputs (horizontal) -->
		            <div class="panel panel-flat">
						<div class="panel-heading">
							<h6 class="panel-title">Transaksi Penjualan</h6>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

		                <div class="panel-body">
		               <form action="" method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Transaksi Penjualan</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">

								<div class="form-group">
									<label class="col-lg-2 control-label">Transaksi ID:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="transaksiID" onkeyup="this.value = this.value.toUpperCase()" value="SMG-<?php echo date("dYis"); ?>" placeholder="">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Tanggal:</label>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar"></i></span>
											<input type="text" class="form-control datepicker" name="tanggal" value="<?php echo date('Y-m-d') ?>" placeholder="Pilih Tanggal&hellip;">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">ID Customer:</label>
									<div class="col-lg-6">
										<select class="select" name="idCustomer" id="idCustomer" onchange="cek_database()" required>
											<optgroup label="Customer ID">
												<option value="">Pilih Customer</option>
												<?php foreach($nasabah as $nsbh){ ?>
												<option value="<?php echo $nsbh['TN_CUSTID']; ?>"><?php echo $nsbh['TN_CUSTID']; ?></option>
												<?php  } ?>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nama Customer:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="nama" id="nama" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Customer">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">No Telp:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="noTelp" id="noTelp" onkeyup="this.value = this.value.toUpperCase()" placeholder="No Telp">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Alamat:</label>
									<div class="col-lg-9">
										<textarea rows="5" cols="5" name="alamat" id="alamat" class="form-control" placeholder="Enter your message here"></textarea>
									</div>
								</div>


								<div class="form-group">
									<label class="col-lg-2 control-label">Type Penjualan:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="typePenjualan" id="typePenjualan" onkeyup="this.value = this.value.toUpperCase()" value="KREDIT" placeholder="Type Penjualan">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Pembiayaan Oleh:</label>
									<div class="col-lg-6">
										<select class="select" name="leasingId">
											<optgroup label="Nama Leasing">
												<option>Pilih Leasing</option>
												<?php if(is_array($leasing)) {
												 foreach($leasing as $row) {
												 ?>
												<option value="<?php echo $row['TL_PRSHID']; ?>"><?php echo $row['TL_PRSH_NAMA']; ?></option>
												<?php } } ?>
											</optgroup>
										</select>
									</div>
								</div>


								<div class="form-group">
									<label class="col-lg-2 control-label">Uang Muka:</label>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Rp.</span>
											<input type="text" name="uangMuka" class="form-control" id="inputku" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" placeholder="Uang Muka">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Tenor:</label>
									<div class="col-lg-6">
										<div class="input-group">
											<input type="text" class="form-control" onkeypress="return hanyaAngka(event)"  name="tenor"  placeholder="Tenor">
											<span class="input-group-addon">/ Bulan</span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Angsuran:</label>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Rp.</span>
											<input type="text" class="form-control" id="inputku" onkeydown="return numbersonly(this, event)" name="angsuran" onkeyup="this.value = this.value.toUpperCase()" placeholder="Angsuran">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Type Unit ID:</label>
									<div class="col-lg-6">
										<select class="select" name="unitId" id="unitId" onchange="cek_unit()">
											<optgroup label="Status">
												<option>Pilih Unit</option>
												<?php if(is_array($unit)) { ?>
												<?php foreach($unit as $units) { ?>
												<option value="<?php echo $units['TU_UNITID']; ?>"><?php echo $units['TU_UNITID']; ?></option>
												<?php }} ?>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Type Unit:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="unitNama" id="unitNama" onkeyup="this.value = this.value.toUpperCase()" placeholder="Type Unit" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Warna:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="unitWarna" id="unitWarna" onkeyup="this.value = this.value.toUpperCase()" placeholder="Warna">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">No Rangka:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="unitRangka" id="unitRangka" onkeyup="this.value = this.value.toUpperCase()" placeholder="No Rangka">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">No Mesin:</label>
									<div class="col-lg-6">
										<input type="text" class="form-control" name="unitMesin" id="unitMesin" onkeyup="this.value = this.value.toUpperCase()" placeholder="No Mesin">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Harga:</label>
									<div class="col-lg-6">
										<div class="input-group">
											<span class="input-group-addon">Rp.</span>
											<input type="text" class="form-control" name="unitHarga" id="unitHarga" onkeyup="this.value = this.value.toUpperCase()" placeholder="Harga">
										</div>
									</div>
								</div>


								<div class="text-right">
									<button name="submit" class="btn btn-primary">Submit<i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>
					    </div>
					</div>
					<!-- /multiple row inputs (horizontal) -->


					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<script type="text/javascript">
	 function hanyaAngka(evt) {
	    var charCode = (evt.which) ? evt.which : event.keyCode
	    if (charCode > 31 && (charCode < 48 || charCode > 57))
	      return false;
	    return true;
	    if(charCode.length != 10) {
	      alert("test");
	      return false;
	    }
	  }

	function cek_database(){
        var custId = $("#idCustomer").val();
       // console.log(custId);
        $.ajax({
            url: 'nasabah-id',
            data:"idCustomer="+custId,
        }).success(function (data) {
            var json = data,
            obj = JSON.parse(json);
            $('#nama').val(obj.nama);
            $('#noTelp').val(obj.noTelp);
            $('#alamat').val(obj.alamat);
        });
    }

    function cek_unit(){
        var unitIdX = $("#unitId").val();
        $.ajax({
            url: 'unit-id',
            data:"unitId="+unitIdX,
        }).success(function(data) {
            var json1 = data,
            obj1 = JSON.parse(json1);
            $('#unitNama').val(obj1.unitNama);
            $('#unitWarna').val(obj1.unitWarna);
            $('#unitRangka').val(obj1.unitRangka);
            $('#unitMesin').val(obj1.unitMesin);
            $('#unitHarga').val(obj1.unitHarga);
        });
    }
</script>
    <script type="text/javascript">
    	function tandaPemisahTitik(b){
			var _minus = false;
			if (b<0) _minus = true;
				b = b.toString();
				b=b.replace(".","");
				b=b.replace("-","");
				c = "";
				panjang = b.length;
				j = 0;
				for (i = panjang; i > 0; i--){
					j = j + 1;
					if (((j % 3) == 1) && (j != 1)){
						c = b.substr(i-1,1) + "." + c;
					} else {
						c = b.substr(i-1,1) + c;
					}
				}
				if (_minus) c = "-" + c ;
					return c;
				}

			function numbersonly(ini, e){
				if (e.keyCode>=49){
					if(e.keyCode<=57){
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
						ini.value = tandaPemisahTitik(b);
						return false;
					} else if(e.keyCode<=105){
						if(e.keyCode>=96){
						//e.keycode = e.keycode - 47;
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
						ini.value = tandaPemisahTitik(b);
						//alert(e.keycode);
						return false;
					} else {return false;}
				} else {
					return false; 
				}
		}else if (e.keyCode==48){
			a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
			b = a.replace(/[^\d]/g,"");
			if (parseFloat(b)!=0){
				ini.value = tandaPemisahTitik(b);
				return false;
			} else {
				return false;
		}
	}else if (e.keyCode==95){
		a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
		b = a.replace(/[^\d]/g,"");
		if (parseFloat(b)!=0){
			ini.value = tandaPemisahTitik(b);
			return false;
		} else {
			return false;
		}
	}else if (e.keyCode==8 || e.keycode==46){
		a = ini.value.replace(".","");
		b = a.replace(/[^\d]/g,"");
		b = b.substr(0,b.length -1);
		if (tandaPemisahTitik(b)!=""){
			ini.value = tandaPemisahTitik(b);
		} else {
			ini.value = "";
		}

		return false;
	} else if (e.keyCode==9){
		return true;
	} else if (e.keyCode==17){
		return true;
	} else {
		//alert (e.keyCode);
		return false;
	}

}

    </script>
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>
