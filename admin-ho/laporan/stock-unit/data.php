<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasilreportunit.xls");
include_once ('./query/model.php');
	$log = new Model();
    $ctrl = $log->ReportStock($_GET["tanggalAwal"], $_GET["tanggalAkhir"]);
?>
<style type="text/css">
	.table1 {
	    font-family: sans-serif;
	    color: #444;
	    border-collapse: collapse;
	    width: 50%;
	    border: 1px solid #f2f5f7;
	}
	 
	.table1 tr th{
	    background: #000;
	    color: #fff;
	    font-weight: 5px;
	}
	 
	.table1, th, td {
	    padding: 5px 20px;
	}

	.table1, td {
		font-size: 14px;
	}
	 
	.table1 tr:hover {
	    background-color: #f5f5f5;
	}

	.table1 tr:nth-child(even) {
	    background-color: #f2f2f2;
	} 

</style>
<table id="example" class="table1" style="width:100%">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal Masuk</th>
			<th>Aging</th>
			<th class="text-center">Unit ID</th>
			<th>Type</th>
			<th>Warna</th>
			<th>No Rangka</th>
			<th class="text-center">No Mesin</th>
			<th>Harga</th>
			<th class="text-center">Tahun</th>
			<th class="text-center">Status</th>
		</tr>
	</thead>
	<tbody>
		<?php
		 $no = 1;
		 $dateNow = date("Y-m-d");
		 if(is_array($ctrl) || is_object($ctrl)) {
		 foreach($ctrl as $row) { ?>
		<tr>
			<td><?= $no++; ?></td>
			<td><?= $log->TanggalIndo($row["TU_TGL_WAREHOUSE"]); ?></td>
			<td><?php $booking    =new DateTime($row['TU_TGL_WAREHOUSE']);
                $today        =new DateTime();
                $diff = $today->diff($booking);
                echo $diff->d; echo " Hari";
            ?></td>
			<td><?= $row["TU_UNITID"]; ?></td>
			<td><?= $row["TU_TYPE"]; ?></td>
			<td><?= $row["TU_WARNA"]; ?></td>
			<td><?= $row["TU_NOMER_RANGKA"]; ?></td>
			<td><?= $row["TU_NOMER_MESIN"]; ?></td>
			<td>Rp.<?= number_format($row["TU_HARGA"],2,',','.'); ?></td>
			<td class="text-center"><?= $row["TU_TAHUN"]; ?></td>
			<?php if($row["TU_STATUS"] == "READY") { ?>
			<td style="background-color: #6ccb45">READY</td>
			<?php } else { ?>
			<td style="background-color: red">KOSONG</td>
			<?php } ?>
		</tr>
		<?php } }?>
	</tbody>
</table>