<?php
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
 session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

	if(isset($_POST['submit'])){
		$leasing['leasingId'] = $_POST['leasingId'];
	    $leasing['namaLeasing'] = $_POST['namaLeasing'];
	    $leasing['noTelp'] = $_POST['noTelp'];
	    // $leasing['jumlahPiutang'] = $_POST['jumlahPiutang'];
	    // $leasing['totalNominal'] = $_POST['totalNominal'];
	   	$leasing['alamat'] = $_POST['alamat'];
	    $ctrl = $log->tambahLeasing($leasing);
	}

	$Leasing = $log->getLeasing();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/picker_date.js"></script>

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/adminoh/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master Data</span> - Leasing</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Master Data</a></li>
							<li class="active">Leasing</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<form action="" method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Tambah Data Leasing</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">

								<div class="form-group">
									<label class="col-lg-2 control-label">ID Leasing:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="leasingId" value="LS121<?php echo rand(11111,99999)."-".date("dis"); ?> " placeholder="Customer ID" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nama Leasing:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="namaLeasing" onkeyup="this.value = this.value.toUpperCase()"  placeholder="Nama Leasing" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">No Telp:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="noTelp" onkeypress="return hanyaAngka(event)" placeholder="No Telp" required>
									</div>
								</div>

								<!-- <div class="form-group">
									<label class="col-lg-2 control-label">Jumlah Piutang (Unit):</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="jumlahPiutang" onkeypress="return hanyaAngka(event)"  placeholder="Jumlah Piutang">
									</div>
								</div> -->

								<!-- <div class="form-group">
									<label class="col-lg-2 control-label">Nominal Piutang:</label>
									<div class="col-lg-9">
										<div class="input-group">
											<span class="input-group-addon">Rp.</span>
											<input type="text" name="totalNominal" class="form-control" id="inputku" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" placeholder="Total Nominal">
										</div>
									</div>
								</div> -->

								<div class="form-group">
									<label class="col-lg-2 control-label">Alamat:</label>
									<div class="col-lg-9">
										<textarea rows="5" cols="5" name="alamat" class="form-control" placeholder="Enter your message here"></textarea>
									</div>
								</div>


								<div class="text-right">
									<button name="submit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Scrollable datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Data Nasabah</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>
						<table class="table datatable-scroll-y" width="100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Leasing ID</th>
									<th>Nama Leasing</th>
									<th>No Telp</th>
									<!-- <th>Jumlah Piutang (Unit)</th> -->
									<!-- <th>Nominal Piutang</th> -->
									<th>Alamat</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 1;
								if(is_array($Leasing) || is_object($Leasing)) {
								 foreach($Leasing as $row) { ?>
								
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row["TL_PRSHID"]; ?></td>
									<td><?= $row["TL_PRSH_NAMA"]; ?></td>
									<td><?= $row["TL_TELPON"]; ?></td>
									<td><?= $row["TL_ALAMAT"]; ?></td>
									<?php if($row["TL_STATUS"] == "AKTIF") { ?>
									<td><span class="label label-success">AKTIF</span></td>
									<?php } else { ?>
									<td><span class="label label-warning">INAKTIF</span></td>
									<?php } ?>
									<td class="text-center">
										<a href="edit-leasing?data-leasing=<?= $row["TL_PRSHID"]; ?>" class="btn btn-warning"><span class="icon-pencil6"></span></a>
										<a href="delete-leasing?data-leasing=<?= $row["TL_PRSHID"]; ?>" class="btn btn-danger delete-link"><span class="icon-trash"></span></a>
									</td>
								</tr>
								<?php } } ?>
							</tbody>
						</table>
					</div>
					<!-- /scrollable datatable -->
					 


					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	 <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: 'Apakah Anda Yakin hapus data?',
                        text: 'Data yang telah dihapus akan hilang',
                        type: 'warning',
                        html: true,
                        confirmButtonColor: '#d9534f',
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
    </script>
    <script type="text/javascript">
    	 function hanyaAngka(evt) {
		    var charCode = (evt.which) ? evt.which : event.keyCode
		    if (charCode > 31 && (charCode < 48 || charCode > 57))
		      return false;
		    return true;
		    if(charCode.length != 10) {
		      alert("Nomer Telpon harus 11 digit");
		      return false;
		    }
		  }

		function tandaPemisahTitik(b){
			var _minus = false;
			if (b<0) _minus = true;
				b = b.toString();
				b=b.replace(".","");
				b=b.replace("-","");
				c = "";
				panjang = b.length;
				j = 0;
				for (i = panjang; i > 0; i--){
					j = j + 1;
					if (((j % 3) == 1) && (j != 1)){
						c = b.substr(i-1,1) + "." + c;
					} else {
						c = b.substr(i-1,1) + c;
					}
				}
				if (_minus) c = "-" + c ;
					return c;
				}

			function numbersonly(ini, e){
				if (e.keyCode>=49){
					if(e.keyCode<=57){
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
						ini.value = tandaPemisahTitik(b);
						return false;
					} else if(e.keyCode<=105){
						if(e.keyCode>=96){
						//e.keycode = e.keycode - 47;
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
						ini.value = tandaPemisahTitik(b);
						//alert(e.keycode);
						return false;
					} else {return false;}
				} else {
					return false; 
				}
		}else if (e.keyCode==48){
			a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
			b = a.replace(/[^\d]/g,"");
			if (parseFloat(b)!=0){
				ini.value = tandaPemisahTitik(b);
				return false;
			} else {
				return false;
		}
	}else if (e.keyCode==95){
		a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
		b = a.replace(/[^\d]/g,"");
		if (parseFloat(b)!=0){
			ini.value = tandaPemisahTitik(b);
			return false;
		} else {
			return false;
		}
	}else if (e.keyCode==8 || e.keycode==46){
		a = ini.value.replace(".","");
		b = a.replace(/[^\d]/g,"");
		b = b.substr(0,b.length -1);
		if (tandaPemisahTitik(b)!=""){
			ini.value = tandaPemisahTitik(b);
		} else {
			ini.value = "";
		}

		return false;
	} else if (e.keyCode==9){
		return true;
	} else if (e.keyCode==17){
		return true;
	} else {
		//alert (e.keyCode);
		return false;
	}

}
    </script>
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

