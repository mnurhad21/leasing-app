-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 30 Jul 2019 pada 21.09
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `leasing`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_alokasi_piutang`
--

CREATE TABLE `tl_alokasi_piutang` (
  `TAP_BIGID` int(11) NOT NULL,
  `TAP_TRANS_ID` varchar(70) DEFAULT NULL,
  `TAP_CUSTID` varchar(70) DEFAULT NULL,
  `TAP_NAMA` varchar(50) DEFAULT NULL,
  `TAP_LEASING_ID` varchar(70) DEFAULT NULL,
  `TAP_UANG_MUKA` decimal(10,0) DEFAULT NULL,
  `TAP_UNITID` varchar(50) DEFAULT NULL,
  `TAP_HARGA_ITEM` decimal(10,0) DEFAULT NULL,
  `TAP_JANGKA_WAKTU` varchar(10) DEFAULT NULL,
  `TAP_HASIL` decimal(10,0) DEFAULT NULL,
  `TAP_HASIL_BUNGA` decimal(10,0) DEFAULT NULL,
  `TAP_TANGGAL` date DEFAULT NULL,
  `TAP_SYS_CREATED` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_alokasi_piutang`
--

INSERT INTO `tl_alokasi_piutang` (`TAP_BIGID`, `TAP_TRANS_ID`, `TAP_CUSTID`, `TAP_NAMA`, `TAP_LEASING_ID`, `TAP_UANG_MUKA`, `TAP_UNITID`, `TAP_HARGA_ITEM`, `TAP_JANGKA_WAKTU`, `TAP_HASIL`, `TAP_HASIL_BUNGA`, `TAP_TANGGAL`, `TAP_SYS_CREATED`) VALUES
(4, 'SMG-2720185935', 'CS11445011-053926 ', 'SETYANINGSIH AYU RINI', 'LS12111457-050843 ', '0', 'SP9369', '30000000', '20', '1500000', '15000', '2018-08-27', '2018-08-27 13:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_cashflow`
--

CREATE TABLE `tl_cashflow` (
  `TC_BIGID` int(11) NOT NULL,
  `TC_TANGGAL` date DEFAULT NULL,
  `TC_TRANS_ID` varchar(80) NOT NULL,
  `TC_NAMA_BANK` varchar(20) DEFAULT NULL,
  `TC_NAMA_AKUN` varchar(50) DEFAULT NULL,
  `TC_TRANS_FROM` varchar(70) DEFAULT NULL,
  `TC_NOMINAL` decimal(10,0) DEFAULT NULL,
  `TC_STATUS` varchar(20) DEFAULT NULL,
  `TC_KETERANGAN` varchar(100) DEFAULT NULL,
  `TC_CREATED_USER` varchar(40) DEFAULT NULL,
  `TC_SYS_CREATED` datetime DEFAULT NULL,
  `TC_SYS_UPDATED` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_leasing`
--

CREATE TABLE `tl_leasing` (
  `TL_BIGID` int(11) NOT NULL,
  `TL_PRSHID` varchar(30) NOT NULL,
  `TL_PRSH_NAMA` varchar(100) NOT NULL,
  `TL_ALAMAT` varchar(100) DEFAULT NULL,
  `TL_TELPON` varchar(13) DEFAULT NULL,
  `TL_STATUS` varchar(20) NOT NULL,
  `TL_JML_PIUTANG` varchar(20) DEFAULT NULL,
  `TL_NOMINAL_PIUTANG` decimal(30,0) DEFAULT '0',
  `TL_CREATED_USER` varchar(40) DEFAULT NULL,
  `TL_SYS_CREATED` datetime DEFAULT NULL,
  `TL_SYS_UPDATED` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_leasing`
--

INSERT INTO `tl_leasing` (`TL_BIGID`, `TL_PRSHID`, `TL_PRSH_NAMA`, `TL_ALAMAT`, `TL_TELPON`, `TL_STATUS`, `TL_JML_PIUTANG`, `TL_NOMINAL_PIUTANG`, `TL_CREATED_USER`, `TL_SYS_CREATED`, `TL_SYS_UPDATED`) VALUES
(2, 'LS12196453-050639 ', 'ADIRA AMBARAWA', 'adira ambarawa semarang', '0898986786', 'AKTIF', '184', '2785740000', 'ADM.001', '2018-08-05 14:07:42', NULL),
(4, 'LS12111457-050843 ', 'FIF JEPARA', 'fif jepara kalinyamat jldldd', '0897986786777', 'AKTIF', '1', '30000000', 'ADM.001', '2018-08-05 14:09:54', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_nasabah`
--

CREATE TABLE `tl_nasabah` (
  `TN_BIGID` int(11) NOT NULL,
  `TN_CARDID` varchar(40) DEFAULT NULL,
  `TN_PANGGILAN` varchar(10) DEFAULT NULL,
  `TN_CUSTID` varchar(50) DEFAULT NULL,
  `TN_NAMA` varchar(60) DEFAULT NULL,
  `TN_TELPON` varchar(13) DEFAULT NULL,
  `TN_ALAMAT` varchar(100) DEFAULT NULL,
  `TN_JK` tinyint(2) DEFAULT NULL,
  `TN_CREATED_USER` varchar(40) DEFAULT NULL,
  `TN_SYS_CREATED` datetime DEFAULT NULL,
  `TN_SYS_UPDATES` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_nasabah`
--

INSERT INTO `tl_nasabah` (`TN_BIGID`, `TN_CARDID`, `TN_PANGGILAN`, `TN_CUSTID`, `TN_NAMA`, `TN_TELPON`, `TN_ALAMAT`, `TN_JK`, `TN_CREATED_USER`, `TN_SYS_CREATED`, `TN_SYS_UPDATES`) VALUES
(2, '214748364734', 'Nyonya', 'CS11445011-053926 ', 'SETYANINGSIH AYU RINI', '089897867765', 'Jl.Pahlawan semarang kota', 0, 'ADM.001', '2018-08-05 02:40:40', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_penjualan`
--

CREATE TABLE `tl_penjualan` (
  `TP_BIGID` int(11) NOT NULL,
  `TP_NOID` varchar(80) NOT NULL,
  `TP_TANGGAL` date DEFAULT NULL,
  `TP_CUST_ID` varchar(70) DEFAULT NULL,
  `TP_NAMACUST` varchar(50) DEFAULT NULL,
  `TP_ALAMAT` varchar(100) DEFAULT NULL,
  `TP_NOHP` varchar(20) DEFAULT NULL,
  `TP_TYPE_PENJUALAN` varchar(40) DEFAULT NULL,
  `TP_LEASING_ID` varchar(70) DEFAULT NULL,
  `TP_DP` decimal(10,0) DEFAULT NULL,
  `TP_TENOR` varchar(20) DEFAULT NULL,
  `TP_ANGSURAN` decimal(20,0) DEFAULT NULL,
  `TP_UNIT_ID` varchar(50) DEFAULT NULL,
  `TP_WARNA` varchar(20) DEFAULT NULL,
  `TP_TYPE_NAMA` varchar(30) DEFAULT NULL,
  `TP_NO_RANGKA` varchar(30) DEFAULT NULL,
  `TP_NO_MESIN` varchar(30) DEFAULT NULL,
  `TP_HARGA` decimal(10,0) DEFAULT NULL,
  `TP_HASIL` decimal(11,0) NOT NULL,
  `TP_STATUS` varchar(20) DEFAULT NULL,
  `TP_CASH_STATUS` varchar(20) DEFAULT NULL,
  `TP_CHECK` varchar(5) DEFAULT NULL,
  `TP_KETERANGAN` varchar(100) DEFAULT NULL,
  `TP_CREATED_USER` varchar(40) DEFAULT NULL,
  `TP_SALES` varchar(50) DEFAULT NULL,
  `TP_UPDATED_USER` varchar(50) DEFAULT NULL,
  `TP_SYS_CREATED` datetime DEFAULT NULL,
  `TP_SYS_UPDATED` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_penjualan`
--

INSERT INTO `tl_penjualan` (`TP_BIGID`, `TP_NOID`, `TP_TANGGAL`, `TP_CUST_ID`, `TP_NAMACUST`, `TP_ALAMAT`, `TP_NOHP`, `TP_TYPE_PENJUALAN`, `TP_LEASING_ID`, `TP_DP`, `TP_TENOR`, `TP_ANGSURAN`, `TP_UNIT_ID`, `TP_WARNA`, `TP_TYPE_NAMA`, `TP_NO_RANGKA`, `TP_NO_MESIN`, `TP_HARGA`, `TP_HASIL`, `TP_STATUS`, `TP_CASH_STATUS`, `TP_CHECK`, `TP_KETERANGAN`, `TP_CREATED_USER`, `TP_SALES`, `TP_UPDATED_USER`, `TP_SYS_CREATED`, `TP_SYS_UPDATED`) VALUES
(4, 'SMG-2720185935', '2018-08-27', 'CS11445011-053926 ', 'SETYANINGSIH AYU RINI', 'Jl.Pahlawan semarang kota', '089897867765', 'KREDIT', 'LS12111457-050843 ', '0', '20', '280000', 'SP9369', 'WHITE BLUE', 'CBR 150 RW', '09089UIHJKG7UYGHJBHJJ', 'BNJGJHG7T878767868899', '30000000', '30000000', 'SURVEY', 'ON_ALOKASI', 'Y', NULL, 'ADM.001', NULL, NULL, '2018-08-27 13:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_setting`
--

CREATE TABLE `tl_setting` (
  `TS_BIGID` int(11) NOT NULL,
  `TS_ID` varchar(50) NOT NULL,
  `TS_VALUE` varchar(100) DEFAULT NULL,
  `TS_CRATED_AT` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_unit`
--

CREATE TABLE `tl_unit` (
  `TU_BIGID` int(11) NOT NULL,
  `TU_UP_ID` varchar(11) DEFAULT NULL,
  `TU_UNITID` varchar(30) NOT NULL,
  `TU_TYPE` varchar(50) DEFAULT NULL,
  `TU_WARNA` varchar(40) DEFAULT NULL,
  `TU_NOMER_RANGKA` varchar(50) DEFAULT NULL,
  `TU_NOMER_MESIN` varchar(50) DEFAULT NULL,
  `TU_HARGA` decimal(10,0) DEFAULT NULL,
  `TU_TAHUN` varchar(10) DEFAULT NULL,
  `TU_CREATED_USER` varchar(40) DEFAULT NULL,
  `TU_STATUS` varchar(20) DEFAULT NULL,
  `TU_TGL_WAREHOUSE` date DEFAULT NULL,
  `TU_SYS_CREATED` datetime DEFAULT NULL,
  `TU_SYS_UPDATED` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_unit`
--

INSERT INTO `tl_unit` (`TU_BIGID`, `TU_UP_ID`, `TU_UNITID`, `TU_TYPE`, `TU_WARNA`, `TU_NOMER_RANGKA`, `TU_NOMER_MESIN`, `TU_HARGA`, `TU_TAHUN`, `TU_CREATED_USER`, `TU_STATUS`, `TU_TGL_WAREHOUSE`, `TU_SYS_CREATED`, `TU_SYS_UPDATED`) VALUES
(10, NULL, 'SP9369', 'CBR 150 RW', 'WHITE BLUE', '09089UIHJKG7UYGHJBHJJ', 'BNJGJHG7T878767868899', '30000000', '2018', 'ADM.OH.001', 'ORDER', '2018-08-26', '2018-08-26 07:31:33', '2018-08-27 13:00:00'),
(13, NULL, 'SP6800', 'SUPRA 125 R', 'MERAH HITAM', 'JEKEHJKEHEJKHEEHJ', 'JDGHDGDHJGE8767568', '20000000', '2018', 'ADM.OH.001', 'READY', '2018-09-01', '2018-09-01 18:48:26', '2018-09-02 21:45:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_unit_data`
--

CREATE TABLE `tl_unit_data` (
  `TUD_ID` int(11) NOT NULL,
  `TUD_NAMA` varchar(40) DEFAULT NULL,
  `TUD_HARGA` decimal(10,0) DEFAULT NULL,
  `TUD_SYS_CREATED` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_unit_data`
--

INSERT INTO `tl_unit_data` (`TUD_ID`, `TUD_NAMA`, `TUD_HARGA`, `TUD_SYS_CREATED`) VALUES
(2, 'CBR 150 RW', '30000000', '2018-08-26 00:14:56'),
(3, 'SUPRA 125 R', '20000000', '2018-09-01 11:38:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_upload`
--

CREATE TABLE `tl_upload` (
  `TP_BIGID` int(11) NOT NULL,
  `TP_VALUE` int(10) DEFAULT NULL,
  `TP_TGL` date DEFAULT NULL,
  `TP_VALUE_PATH` text,
  `TP_USER_ID` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_upload`
--

INSERT INTO `tl_upload` (`TP_BIGID`, `TP_VALUE`, `TP_TGL`, `TP_VALUE_PATH`, `TP_USER_ID`) VALUES
(8, 903, '2018-08-13', 'library/document/file-1534110741.xlsx', 'ADM.OH.001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tl_user`
--

CREATE TABLE `tl_user` (
  `U_BIGID` int(11) NOT NULL,
  `U_ID` varchar(40) NOT NULL,
  `U_FULLNAME` varchar(60) DEFAULT NULL,
  `U_EMAIL` varchar(60) DEFAULT NULL,
  `U_TELPON` varchar(20) DEFAULT NULL,
  `U_SETTING_FORGOT` varchar(60) DEFAULT NULL,
  `U_FORGOT_PASS` varchar(80) DEFAULT NULL,
  `U_PASSWORD_HASH` varchar(80) NOT NULL,
  `U_GROUP_RULE` varchar(30) NOT NULL,
  `U_STATUS` varchar(20) NOT NULL,
  `U_PRSH_ID` varchar(50) DEFAULT NULL,
  `U_LOGIN_TOKEN` varchar(100) DEFAULT NULL,
  `U_LOGIN_WAKTU` datetime DEFAULT NULL,
  `U_DEFAULT_LOGIN` text,
  `U_IP_POSITION` varchar(30) DEFAULT NULL,
  `U_LOGOUT_WAKTU` datetime DEFAULT NULL,
  `U_AVATAR` text,
  `U_BACKGROUND` text,
  `U_CREATED_USER` varchar(40) DEFAULT NULL,
  `U_KEY` varchar(100) DEFAULT NULL,
  `U_SYS_CREATED` datetime DEFAULT NULL,
  `U_SYS_UPDATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tl_user`
--

INSERT INTO `tl_user` (`U_BIGID`, `U_ID`, `U_FULLNAME`, `U_EMAIL`, `U_TELPON`, `U_SETTING_FORGOT`, `U_FORGOT_PASS`, `U_PASSWORD_HASH`, `U_GROUP_RULE`, `U_STATUS`, `U_PRSH_ID`, `U_LOGIN_TOKEN`, `U_LOGIN_WAKTU`, `U_DEFAULT_LOGIN`, `U_IP_POSITION`, `U_LOGOUT_WAKTU`, `U_AVATAR`, `U_BACKGROUND`, `U_CREATED_USER`, `U_KEY`, `U_SYS_CREATED`, `U_SYS_UPDATE`) VALUES
(1, 'ADM.001', 'ADMINISTRATOR DEALER ', 'admin@dealer.com', '08387397378', NULL, NULL, '25d55ad283aa400af464c76d713c07ad', 'GR_ADMIN_DEALER', 'USER_ACTIVE', '-', '3696e40b3c234975e9173aa506c919', '2019-01-17 19:30:00', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '127.0.0.1', '2019-01-17 19:30:08', '01082018015832WhatsAppImage2018-07-25at9.28.14PM.jpeg', '01082018022408user-img-background.jpg', NULL, NULL, '2018-07-31 00:20:07', NULL),
(2, 'ADM.OH.001', 'NISA DAMAYANTI', 'nisa@admin.com', '089787678889', NULL, NULL, '25d55ad283aa400af464c76d713c07ad', 'GR_ADMIN_HEAD', 'USER_ACTIVE', '-', '0d6777b24929a314fdd7ef70b0ef27', '2018-11-15 21:19:16', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', '::1', '2018-11-01 20:56:41', NULL, NULL, NULL, 'QURNLk9ILjAwMTIwMTgwODA1MDYyODAzOjAzOjAw', '2018-07-31 03:03:00', NULL),
(4, 'BUDI123', 'BUDI ABDI NEGARA', 'budi1@gmail.com', '089897867765', NULL, NULL, '25d55ad283aa400af464c76d713c07ad', 'GR_ADMIN_LEASING', 'USER_ACTIVE', 'LS12111457-050843', 'b0d688a0bd580e2f599b89442fb339', '2019-01-17 19:30:36', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36', '127.0.0.1', '2018-08-26 17:37:46', NULL, NULL, 'ADM.001', 'QURNLjAwMTIwMTgtMDgtMDUgMDc6MDA6NDA=', '2018-08-05 07:00:40', NULL),
(5, 'HADI12', 'ADMIN HADI', 'hadi@admin.com', '089786677778', NULL, NULL, '25d55ad283aa400af464c76d713c07ad', 'GR_ADMIN_LEASING', 'USER_ACTIVE', 'LS12196453-050639 ', '3f17b280afb68ac6f1a365ca74719f', '2018-08-26 23:10:25', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36', '127.0.0.1', '2018-08-26 23:26:34', NULL, NULL, 'ADM.OH.001', 'QURNLk9ILjAwMTIwMTgtMDgtMjYgMTc6NDI6Mjc=', '2018-08-26 17:42:27', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tl_alokasi_piutang`
--
ALTER TABLE `tl_alokasi_piutang`
  ADD PRIMARY KEY (`TAP_BIGID`);

--
-- Indeks untuk tabel `tl_cashflow`
--
ALTER TABLE `tl_cashflow`
  ADD PRIMARY KEY (`TC_BIGID`);

--
-- Indeks untuk tabel `tl_leasing`
--
ALTER TABLE `tl_leasing`
  ADD PRIMARY KEY (`TL_BIGID`);

--
-- Indeks untuk tabel `tl_nasabah`
--
ALTER TABLE `tl_nasabah`
  ADD PRIMARY KEY (`TN_BIGID`);

--
-- Indeks untuk tabel `tl_penjualan`
--
ALTER TABLE `tl_penjualan`
  ADD PRIMARY KEY (`TP_BIGID`);

--
-- Indeks untuk tabel `tl_setting`
--
ALTER TABLE `tl_setting`
  ADD PRIMARY KEY (`TS_BIGID`);

--
-- Indeks untuk tabel `tl_unit`
--
ALTER TABLE `tl_unit`
  ADD PRIMARY KEY (`TU_BIGID`);

--
-- Indeks untuk tabel `tl_unit_data`
--
ALTER TABLE `tl_unit_data`
  ADD PRIMARY KEY (`TUD_ID`);

--
-- Indeks untuk tabel `tl_upload`
--
ALTER TABLE `tl_upload`
  ADD PRIMARY KEY (`TP_BIGID`);

--
-- Indeks untuk tabel `tl_user`
--
ALTER TABLE `tl_user`
  ADD PRIMARY KEY (`U_BIGID`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tl_alokasi_piutang`
--
ALTER TABLE `tl_alokasi_piutang`
  MODIFY `TAP_BIGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tl_cashflow`
--
ALTER TABLE `tl_cashflow`
  MODIFY `TC_BIGID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tl_leasing`
--
ALTER TABLE `tl_leasing`
  MODIFY `TL_BIGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tl_nasabah`
--
ALTER TABLE `tl_nasabah`
  MODIFY `TN_BIGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tl_penjualan`
--
ALTER TABLE `tl_penjualan`
  MODIFY `TP_BIGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `tl_setting`
--
ALTER TABLE `tl_setting`
  MODIFY `TS_BIGID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tl_unit`
--
ALTER TABLE `tl_unit`
  MODIFY `TU_BIGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `tl_unit_data`
--
ALTER TABLE `tl_unit_data`
  MODIFY `TUD_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tl_upload`
--
ALTER TABLE `tl_upload`
  MODIFY `TP_BIGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `tl_user`
--
ALTER TABLE `tl_user`
  MODIFY `U_BIGID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
