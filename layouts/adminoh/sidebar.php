<div class="sidebar-content">
	<!-- User menu -->
	<div class="sidebar-user">
		<div class="category-content">
			<div class="media">
				<a href="#" class="media-left"><img src="./layouts/asset/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
				<div class="media-body">
					<span class="media-heading text-semibold"><?php echo $_SESSION["U_FULLNAME"]; ?></span>
					<div class="text-size-mini text-muted">
						<i class="icon-pin text-size-small"></i> &nbsp;Admin Head
					</div>
				</div>

				<div class="media-right media-middle">
					<ul class="icons-list">
						<li>
							<a href="#"><i class="icon-cog3"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- /user menu -->

	<!-- Main navigation -->
	<div class="sidebar-category sidebar-category-visible">
		<div class="category-content no-padding">
			<ul class="navigation navigation-main navigation-accordion">

				<!-- Main -->
				<li class="navigation-header"><span style="color: #fff">DASHBOARD ADMINISTRATOR OH</span> <i class="icon-menu" title="Main pages"></i></li>
				<li class="active"><a href="dashboard-adm-oh"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
				<!-- <li><a href="stock-unit"><i class="icon-list-unordered"></i> <span>Stock Unit</span></a></li> -->
				<li>
					<a href="#"><i class="icon-list-unordered"></i> <span>Data Master</span></a>
					<ul>
						<li><a href="leasing"><span class="icon-office"></span> Data Leasing</a></li>
						<li><a href="management-user"><span class="icon-user-tie"></span> Manajemen User</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="icon-list-unordered"></i> <span>Stock Unit</span></a>
					<ul>
						<li><a href="stock-unit"><i class="icon-list-unordered"></i>Data Stock Unit</a></li>
						<li><a href="unit-first"><i class="icon-list-unordered"></i>Data Unit</a></li>
					</ul>
				</li>
				<li>
					<a href="#"><i class="icon-stack2"></i> <span>Transaksi</span></a>
					<ul>
						<li><a href="cashflow"><i class="icon-cash4"></i> Cashflow</a></li>
					</ul>
				</li>
				<li><a href="alokasi-piutang"><i class="icon-calculator2"></i> <span>Alokasi Piutang</span></a></li>
				<li><a href="order-list"><i class="icon-list-unordered"></i> <span>Data Order</span></a></li>
				<li>
					<a href="#"><i class="icon-stack"></i> <span>Laporan</span></a>
					<ul>
						<li><a href="report-stock-unit"><span class="icon-database-refresh"></span> Stock Unit</a></li>
						<li><a href="report-piutang-leasing"><span class=" icon-credit-card"></span> Piutang Leasing</a></li>
						<li><a href="report-piutang-followup"><span class="icon-user-check"></span> Follow Up Leasing</a></li>
						<li><a href="report-penjualan"><span class="icon-bike"></span> Penjualan Unit</a></li>
					</ul>
				</li>
				<!-- /main -->

				<!-- Forms -->
				<li class="navigation-header"><span>SETTING INFORMATION</span> <i class="icon-user-tie" title="Forms"></i></li>
				<!-- <li><a href="changelog.html"><i class="icon-list-unordered"></i> <span>Manajemen User</span></a></li> -->
				<li><a href="my-profile"><i class="icon-user-tie"></i> <span>Profil</span></a></li>
				<li><a href="setting-account"><i class="icon-gear"></i> <span>Setting</span></a></li>
				<!-- /forms -->
				<!-- /appearance -->

				<!-- /page kits -->

			</ul>
		</div>
	</div>
	<!-- /main navigation -->
</div>