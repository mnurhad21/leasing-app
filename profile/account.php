<?php
session_start(); 
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {

include_once ('./query/model.php');
$log = new Model();

if(isset($_POST['masuk']) && $_FILES['IMG_PATH']){
	$data1['userId'] = $_POST['userId'];
    $data1['name'] = $_POST['name'];
    $data1['email'] = $_POST['email'];
    $data1['password'] = md5($_POST['password']);
    $data1['IMG_PATH'] = $_FILES['IMG_PATH']['name'];
    $ctrl = $log->accountSetting($data1);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color: #bb1b05">
				<?php 
				if($_SESSION["U_GROUP_RULE"] == "GR_ADMIN_DEALER") {
					include_once './layouts/admindealer/sidebar.php';
				} elseif($_SESSION["U_GROUP_RULE"] == "GR_ADMIN_HEAD") {
				 	include_once './layouts/adminoh/sidebar.php';
				} else {
					include_once './layouts/adminleasing/sidebar.php';
				}
				?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Account Setting</span> - <?php echo $_SESSION["U_FULLNAME"]; ?></h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="form_layout_horizontal.html">Account Setting</a></li>
							<li class="active">User</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Horizontal form options -->
					<div class="row">
						<div class="col-md-10">

							<!-- Basic layout-->
							<form action="" method="post" class="form-horizontal" id="myForm" enctype="multipart/form-data">
								<div class="panel panel-flat">
									<div class="panel-heading">
										<h5 class="panel-title">Account Setting</h5>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

									<div class="panel-body">

										<div class="form-group">
											<label class="col-lg-3 control-label">User ID:</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" name="userId" value="<?php echo $_SESSION["U_ID"]; ?>" disabled="disabled">
											</div>
										</div>
										<input type="hidden" name="userId" value="<?php echo $_SESSION["U_ID"]; ?>">
										<div class="form-group">
											<label class="col-lg-3 control-label">Name:</label>
											<div class="col-lg-9">
												<input type="text" name="name" onkeyup="this.value = this.value.toUpperCase()" class="form-control" placeholder="Isikan Nama anda" value="<?php echo $_SESSION["U_FULLNAME"]; ?>">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Email:</label>
											<div class="col-lg-9">
												<input type="text" name="email" class="form-control" value="<?php echo $_SESSION["U_EMAIL"]; ?>">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Password lama:</label>
											<div class="col-lg-9">
												<input type="password" class="form-control" placeholder="Isikan password lama">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Password Baru:</label>
											<div class="col-lg-9">
												<input type="password" name="password" class="form-control" placeholder="Isikan password baru">
											</div>
										</div>

										<div class="form-group">
											<label class="col-lg-3 control-label">Avatar:</label>
											<div class="col-lg-9">
												<input type="file" name="IMG_PATH" class="file-styled">
												<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
											</div>
										</div>

										<div class="text-right">
											<input class="btn btn-danger" type="button" onclick="myFunction()" value="Reset">
											<button class="btn btn-primary" name="masuk">Simpan<i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
								</div>
							</form>
							<!-- /basic layout -->

						</div>
					</div>
					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
<script type="text/javascript">
	function myFunction() {
    	document.getElementById("myForm").reset();
	}
</script>

</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>
