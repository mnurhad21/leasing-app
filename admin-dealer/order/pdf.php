<?php
header("Content-type: application/PDF");
session_start();
ob_start();
include_once ('./query/model.php');
$log = new Model();
$cetak = $log->getPDF($_GET["data-id"]);
$date = date("Y-m-d");
 ?>
 <page>
    <style type="text/css">
        table, th, td {
            border: 0px solid black;
        }
        
        .absolute {
            position: absolute;
            top: 40px;
            left: 0;
            width: 50px;
            height: 50px;
        }

        .ccc {
            text-align: center;
        }
    </style>
    <table cellpadding='1'>
        <tbody>
            <tr>
                <td colspan="3" style="text-align: center;">
                    <br><br>
                        <span class="ccc" style="text-align: center; font-family: Verdana; font-size: x-medium;"><b>CV. MITRA ANGKASA JAYA UTAMA</b><br></span>
                        <span>jl. Jendral Sudirman No 130 Langensari Ungaran<br></span>
                        <span>(024) 6926770</span><br><br>
                        <span>================================================================</span>           
                </td>
            </tr>
             <tr>    
                  <td colspan="" style="padding-left: 10px">
                     <div align="left">
                         <br>
                         <table>
                             <tr>
                                 <td><span style='font-family: Verdana; text-align: center; font-size: 13px;'><b>Tanggal</b></span></td>
                                 <td><span style='font-family: Verdana; text-align: center; font-size: 13px;'>:</span></td>
                                 <td><span style='font-family: Verdana; text-align: center; font-size: 13px;'><?= $log->TanggalIndo($cetak["TP_TANGGAL"]); ?></span></td>
                             </tr>
                             <tr>
                                <td><span style='font-family: Verdana; text-align: center; font-size: 13px;'><b>No Transaksi</b></span></td>
                                <td><span style='font-family: Verdana; text-align: center; font-size: 13px;'>:</span></td>
                                <td><span style='font-family: Verdana; text-align: center; font-size: 13px;'><?= $cetak["TP_NOID"]; ?></span></td>
                              </tr>
                         </table>
                     </div> 
                  </td> 
                  <td colspan=''>
                    <div align="left">
                         <br>
                         <table border="1" align="right">
                             <tr>
                                 <td><span style='font-family: Verdana; text-align: left; font-size: 13px;'><b>Kepada Yth</b> :</span><br></td>
                             </tr>
                             <tr>
                                <td><span style='font-family: Verdana; text-align: center; font-size: 11px;'><?= $cetak["TP_NAMACUST"]; ?></span><br><?= $cetak["TP_ALAMAT"]; ?><br></td>
                              </tr>
                         </table>
                     </div> 
                  </td> 
              </tr>
               <tr>     
                 <td colspan='3'>
                    <br>
                    <span style='font-size: x-medium;'>Dengan ini kami kirimkan 1 unit sepeda motor :</span>
                 </td>     
              </tr>
              <tr>
                  <td align='top' colspan='3'>
                      <div align='left' style='margin-left: 0px'>
                          <br>
                          <table style='margin-left: 4px;'>
                              <tbody>
                                  <tr>           
                                       <td width='80'><span style='font-size: x-medium;'>Type Unit</span></td>       <td width='10'><span style='font-size: x-medium;'>:</span></td>           
                                       <td width='248'><span style='font-size: x-medium;'><?= $cetak["TP_TYPE_NAMA"]; ?></span>
                                       </td>         
                                  </tr>
                                  <tr>           
                                       <td><span style='font-size: x-medium;'>Warna</span></td>           
                                       <td><span style='font-size: x-medium;'>:</span></td>           
                                       <td><span style='font-size: x-medium;'><?= $cetak["TP_WARNA"]; ?></span></td>         
                                  </tr>
                                  <tr>           
                                       <td><span style='font-size: x-medium;'>No. Rangka</span></td>           
                                       <td><span style='font-size: x-medium;'>:</span></td>           
                                       <td><span style='font-size: x-medium;'><?= $cetak["TP_NO_RANGKA"]; ?></span></td>         
                                  </tr>
                                  <tr>           
                                       <td><span style='font-size: x-medium;'>No. Mesin</span></td>           
                                       <td><span style='font-size: x-medium;'>:</span></td>           
                                       <td><span style='font-size: x-medium;'><?= $cetak["TP_NO_MESIN"]; ?></span></td>         
                                  </tr>
                              </tbody>
                          </table>
                      </div><br><br>
                  </td>
              </tr>
                <tr>     
                   <td align="top" colspan='3'>
                      <table border="1" cellpadding="3" cellspacing="3">
                        <tr>
                          <td>Harga Unit</td>
                          <td>:</td>
                          <td>Rp. <?= number_format($cetak['TP_HARGA'],2,',','.'); ?></td>
                        </tr>
                        <tr>
                          <td>Angsuran</td>
                          <td>:</td>
                          <td>Rp. <?= number_format($cetak['TP_ANGSURAN'],2,',','.')." x ".$cetak["TP_TENOR"]; ?></td>
                        </tr>
                         <tr>
                          <td>DP yang harus dibayar</td>
                          <td>:</td>
                          <td>Rp. <?= number_format($cetak['TP_DP'],2,',','.'); ?></td>
                        </tr>
                      </table>
                      <br>
                      <br>
                   </td>   
              </tr>
              <tr>    
                 <td width='30'>
                    <br>
                    <br>
                    <br>
                    <div align='left'>
                      <span style='font-size: x-medium'>Diterima</span>
                    </div>
                    <div align='left'>
                    </div>
                    <br>
                    <br>
                    <br>
                     <div align='left'>
                          <span style='font-size: 11px;'><?= $cetak["TP_NAMACUST"]; ?></span>
                      </div>
                 </td>     
                 <td width="30">
                    <br>
                    <br>
                    <br><br>
                    <div align='left'>
                      <span style='font-size: x-medium'>Disetujui</span>
                    </div>
                    <div align='left'>
                    </div>
                    <br>
                    <br>
                    <br>
                     <div align='left'>
                          <span style='font-size: x-medium;'>Deny Wahyu Kristyanto</span>
                      </div>
                 </td>    
                  <td width="90">
                    <p><b>Ungaran,<?= date("d/m/Y"); ?></b></p>
                    <div align='left'>
                      <span style='font-size: x-medium'>Hormat Kami</span>
                    </div>
                    <div align='left'>
                    </div>
                    <br>
                    <br>
                     <div align='left'>
                          <span style='font-size: 11px;'><?= $_SESSION["U_FULLNAME"]; ?></span>
                      </div>
                 </td>      
              </tr>
        </tbody>
    </table>
 </page>
 <?php
$filename="mhs.pdf";
$content = ob_get_clean();
require_once './library/html2pdf/html2pdf.class.php';

$html2pdf = new HTML2PDF('P','A5','en', false, 'ISO-8859-15',array(5, 0, 0, 0));
$html2pdf->setDefaultFont('Arial');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->writeHTML($content);
ob_end_clean();
$html2pdf->Output($filename, 'I');
?>