<?php
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
 session_start();
 if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
 	include_once ('./query/model.php');
	$log = new Model();

	$user = $log->getAllUser();
	$Leasing = $log->getLeasing();
	$nasabah = $log->getNasabah();
	$unit = $log->getUnit();
	$cashflow = $log->getSumCashflow();
	$piutang = $log->getSumPiutang();

	//char
	//chart
    $data1 = $log->get_chart_penjualan('01');
    $data2 = $log->get_chart_penjualan('02');
    $data3 = $log->get_chart_penjualan('03');
    $data4 = $log->get_chart_penjualan('04');
    $data5 = $log->get_chart_penjualan('05');
    $data6 = $log->get_chart_penjualan('06');
    $data7 = $log->get_chart_penjualan('07');	
    $data8 = $log->get_chart_penjualan('08');
    $data9 = $log->get_chart_penjualan('09');
    $data10 = $log->get_chart_penjualan('10');
    $data11 = $log->get_chart_penjualan('11');
    $data12 = $log->get_chart_penjualan('12');	
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/visualization/echarts/echarts.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/dashboard.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/charts/echarts/lines_areas.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/admindealer/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					<div class="row">
						<div class="col-lg-12">
							<!-- Traffic sources -->
							<div class="panel panel-flat">
								<br>
								<div class="panel-heading">
									<h6 class="panel-title">Dashboard Administrator</h6>
									<div class="heading-elements">
										<span><b>Jam Sekarang : </b></span><span class="jam"></span>
										<style type="text/css">
											.jam {
										        font-size: 1em;
										        background-color: #bdc3c7;
										        border: 1px solid #d35400;
										        border-radius: 2px;
										        padding: 10px;
										     }
										</style>
									</div>
								</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-lg-3">
										<!-- Current server load -->
											<div class="panel" style="background-color: #054c86; color: #fff">
												<div class="panel-body">
													<div class="heading-elements">
														<ul class="icons-list">
											          		<li class="dropdown">
											          			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
																	<ul class="dropdown-menu dropdown-menu-right">
																		<li><a href="data-leasing"><i class="icon-list-unordered"></i> Lihat Detail</a></li>
																	</ul>
											          		</li>
											          	</ul>
													</div>

													<h3 class="no-margin"><?= count($Leasing); ?></h3>
													Jumlah Leasing
												<a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
											</div>
										</div>
										<div class="col-lg-3">
										<!-- Current server load -->
										<div class="panel bg-primary-400">
											<div class="panel-body">
												<div class="heading-elements">
													<ul class="icons-list">
										          		<li class="dropdown">
										          			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
																<ul class="dropdown-menu dropdown-menu-right">
																	<li><a href="list-nasabah"><i class="icon-list-unordered"></i> Lihat Detail</a></li>
																</ul>
										          		</li>
										          	</ul>
												</div>

												<h3 class="no-margin"><i class=""></i><?= count($nasabah); ?></h3>
												Jumlah Customer
											<a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
										</div>
										<!-- /current server load -->
									</div>
									<div class="col-lg-3">
										<!-- Current server load -->
										<div class="panel bg-info-400">
											<div class="panel-body">
												<div class="heading-elements">
													<ul class="icons-list">
										          		<li class="dropdown">
										          			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
																<ul class="dropdown-menu dropdown-menu-right">
																	<li><a href="mngmt-user"><i class="icon-list-unordered"></i> Lihat Detail</a></li>
																</ul>
										          		</li>
										          	</ul>
												</div>

												<h3 class="no-margin"><i class="icon-user-check"></i><?= count($user); ?></h3>
												Jumlah User Pengguna Aktif
											<a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
										</div>
										<!-- /current server load -->
									</div>
									<div class="col-lg-3">
										<!-- Current server load -->
											<div class="panel" style="background-color: #82e1e2; color: #fff;">
												<div class="panel-body">
													<div class="heading-elements">
														<ul class="icons-list">
											          		<li class="dropdown">
											          			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog3"></i> <span class="caret"></span></a>
																	<ul class="dropdown-menu dropdown-menu-right">
																		<li><a href=""><i class="icon-list-unordered"></i> Lihat Detail</a></li>
																	</ul>
											          		</li>
											          	</ul>
													</div>

													<h3 class="no-margin"><?= count($unit); ?></h3>
													Jumlah Unit
												<a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
											</div>
										</div>
										<!-- /current server load -->
									</div>

										<div class="col-lg-3">
											<ul class="list-inline text-center">
												<li>
													<a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-user-tie"></i></a>
												</li>
												<li class="text-left">
													<div class="text-semibold">Jumlah Leasing</div>
													<div class="text-muted"><span class="status-mark border-success position-left"></span><?= count($Leasing); ?> Leasing</div>
												</li>
											</ul>

											<div class="col-lg-10 col-lg-offset-1">
												<div class="content-group" id="new-sessions"></div>
											</div>
										</div>

										<div class="col-lg-3">
											<ul class="list-inline text-center">
												<li>
													<a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-users4"></i></a>
												</li>
												<li class="text-left">
													<div class="text-semibold">Jumlah Customer</div>
													<div class="text-muted"><span class="status-mark border-success position-left"></span> <?= count($nasabah); ?> Customer</div>
												</li>
											</ul>

											<div class="col-lg-10 col-lg-offset-1">
												<div class="content-group" id="total-online"></div>
											</div>
										</div>
										<div class="col-lg-3">
											<ul class="list-inline text-center">
												<li>
													<a href="#" class="btn border-indigo-400 text-indigo-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-user-check"></i></a>
												</li>
												<li class="text-left">
													<div class="text-semibold">User Pengguna Aktif</div>
													<div class="text-muted"><span class="status-mark border-success position-left"></span><?= count($user); ?> User Aktif</div>
												</li>
											</ul>

											<div class="col-lg-10 col-lg-offset-1">
												<div class="content-group" id="new-sessions"></div>
											</div>
										</div>
										<div class="col-lg-3">
											<ul class="list-inline text-center">
												<li>
													<a href="#" class="btn border-teal-400 text-teal-400 btn-flat btn-rounded btn-icon btn-xs valign-text-bottom"><i class="icon-bike"></i></a>
												</li>
												<li class="text-left">
													<div class="text-semibold">Jumlah Unit</div>
													<div class="text-muted"><span class="status-mark border-success position-left"></span><?= count($unit); ?> Unit</div>
												</li>
											</ul>

											<div class="col-lg-10 col-lg-offset-1">
												<div class="content-group" id="new-sessions"></div>
											</div>
										</div>

									</div>
								</div>
							</div>
							<!-- /traffic sources -->
						</div>
						<br>
						 <div class="col-lg-12 text-center">
					       <label class="label label-success">Line Chart Penjualan Unit Per bulan</label>
					      <div id="myfirstchart" style="height: 250px;"></div>
					    </div>
					</div>
					<!-- /main charts -->
					<!-- Dashboard content -->
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
<script type="text/javascript">

	 var morrisCharts = function() {

            var months = ['Januari', 'Febrari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

                Morris.Line({
                  element: 'myfirstchart',
                  data: [
                    { month: "<?php echo date('Y')?>-01", a: "<?= $data1; ?>"},
                    { month: "<?php echo date('Y')?>-02", a: "<?= $data2; ?>"},
                    { month: "<?php echo date('Y')?>-03", a: "<?= $data3; ?>"},
                    { month: "<?php echo date('Y')?>-04", a: "<?= $data4; ?>"},
                    { month: "<?php echo date('Y')?>-05", a: "<?= $data5; ?>"},
                    { month: "<?php echo date('Y')?>-06", a: "<?= $data6; ?>"},
                    { month: "<?php echo date('Y')?>-07", a: "<?= $data7; ?>"},
                    { month: "<?php echo date('Y')?>-08", a: "<?= $data8; ?>"},
                    { month: "<?php echo date('Y')?>-09", a: "<?= $data9; ?>"},
                    { month: "<?php echo date('Y')?>-10", a: "<?= $data10; ?>"},
                    { month: "<?php echo date('Y')?>-11", a: "<?= $data11; ?>"},
                    { month: "<?php echo date('Y')?>-12", a: "<?= $data12; ?>"}
                  ],
                  xkey: 'month',
                  ykeys: ['a'],
                  labels: ['Unit'],
                   xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
                    var month = months[x.getMonth()];
                    return month;
                  },
                  dateFormat: function(x) {
                    var month = months[new Date(x).getMonth()];
                    return month;
                  },
                  resize: true,
                  lineColors: ['#33414E']
                }); }();
</script>
<script type="text/javascript">
    function jam() {
    var time = new Date(),
        hours = time.getHours(),
        minutes = time.getMinutes(),
        seconds = time.getSeconds();
    document.querySelectorAll('.jam')[0].innerHTML = harold(hours) + ":" + harold(minutes) + ":" + harold(seconds);
      
    function harold(standIn) {
        if (standIn < 10) {
          standIn = '0' + standIn
        }
        return standIn;
        }
    }
    setInterval(jam, 1000);
</script>
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

