<?php
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
 session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

	if(isset($_POST['submit'])){
		$dataNasabah['customerId'] = $_POST['customerId'];
	    $dataNasabah['identitasId'] = $_POST['identitasId'];
	    $dataNasabah['panggilan'] = $_POST['panggilan'];
	    $dataNasabah['nama'] = $_POST['nama'];
	    $dataNasabah['gender'] = $_POST['gender'];
	   	$dataNasabah['noTelp'] = $_POST['noTelp'];
	   	$dataNasabah['alamat'] = $_POST['alamat'];
	    $ctrl = $log->updateNasabah($dataNasabah);
	}

	$customer = $log->getNasabahId($_GET["data-customer"]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/picker_date.js"></script>

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/admindealer/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Edit Data</span> - Customer</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Edit Data</a></li>
							<li class="active">Customer</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<form action="" method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Edit Data Customer</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">

								<div class="form-group">
									<label class="col-lg-2 control-label">Customer ID:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="customerId" value="<?php echo $customer["TN_CUSTID"] ?>" disabled placeholder="Customer ID">
										<input type="hidden" name="customerId" value="<?php echo $customer["TN_CUSTID"] ?>">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Identitas ID:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" value="<?php echo $customer["TN_CARDID"] ?>"  name="identitasId"  onkeypress="return hanyaAngka(event)"  placeholder="Identitas ID">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Panggilan:</label>
									<div class="col-lg-9">
										<select class="select" name="panggilan">		
												<option value="Tuan"<?php if($customer["TN_PANGGILAN"] == "Tuan") {echo "selected";} ?>>Tuan</option>
												<option value="Nyonya" <?php if($customer["TN_PANGGILAN"] == "Nyonya") {echo "selected";} ?>>Nyonya</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nama Customer:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="nama" value="<?php echo $customer["TN_NAMA"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Customer">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Jenis Kelamin:</label>
									<div class="col-lg-9">
										<label class="radio-inline">
											<input type="radio" class="styled" name="gender" <?php if (isset($gender) && $gender=="1") echo "checked";?>  value="1" <?php if($customer["TN_JK"] == 1) {echo "checked";} ?>>
											Laki - Laki
										</label>

										<label class="radio-inline">
											<input type="radio" class="styled" name="gender" <?php if (isset($gender) && $gender=="0") echo "checked";?> value="0" <?php if($customer["TN_JK"] == 0) {echo "checked";} ?>>
											Perempuan
										</label>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">No Telp:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" value="<?php echo $customer["TN_TELPON"]; ?>" name="noTelp" onkeypress="return hanyaAngka(event)" placeholder="No Telp">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Alamat:</label>
									<div class="col-lg-9">
										<textarea rows="5" cols="5" name="alamat" class="form-control" placeholder="Enter your message here"><?php echo $customer["TN_ALAMAT"]; ?></textarea>
									</div>
								</div>


								<div class="text-right">
									<a href="list-nasabah" class="btn btn-danger">Cancel <i class="icon-undo2"></i></a>
									<button name="submit" class="btn btn-primary">Update <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
    <script type="text/javascript">
    	 function hanyaAngka(evt) {
		    var charCode = (evt.which) ? evt.which : event.keyCode
		    if (charCode > 31 && (charCode < 48 || charCode > 57))
		      return false;
		    return true;
		    if(charCode.length != 10) {
		      alert("Nomer Telpon harus 11 digit");
		      return false;
		    }
		  }
    </script>
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

