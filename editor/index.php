<?php session_start();
include_once ('./query/model.php');
$log = new Model();

// if(isset($_POST['g-recaptcha-response']) && isset($_POST['masuk'])) {
// 	$captcha = $_POST['g-recaptcha-response'];
// 	if (!$captcha) {
// 	  echo "<script>alert('Tolong cek akses anda dengan reCAPTCHA box.')</script>";
// 	} else {
// 	  $secret = '6LcZL2gUAAAAAHQywruSyKJvMNL1nvERdPFSOXFH';
// 	  $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
// 	  if ($response != true) {
// 	    echo "<script>alert('lengkapi parameter anda')</script>";
// 	  } else {
// 	    $data['loginEmail'] = $_POST['loginEmail'];
// 	    $data['loginPassword'] = md5($_POST['loginPassword']);
// 	    $ctrlLogin = $log->mobileLogin($data);
// 	  }
// 	}
// }

if(isset($_POST['masuk'])) {
    $data['loginEmail'] = $_POST['loginEmail'];
    $data['loginPassword'] = md5($_POST['loginPassword']);
    $ctrlLogin = $log->mobileLogin($data);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- captcha -->
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/login.js"></script>
	<!-- /theme JS files -->
	<script type="text/javascript">
		window.history.forward();
    	function noBack() { window.history.forward(); }
	</script>

</head>
 

<body class="bg-slate-800" style="background-color: #bb1b05" onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="">

	<!-- Page container -->
	<div class="page-container login-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Advanced login -->
					 <form action="" class="form-horizontal" method="post">
						<div class="panel panel-body login-form">
							<div class="text-center">
								<a href=""><img src="./layouts/asset/assets/images/logoapp.jpeg" width="75%"></a>
							</div>
							<br><br>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="loginEmail" placeholder="Email / No Telp">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<div class="input-group">
									<input type="password" class="form-control" name="loginPassword" id="loginPassword" placeholder="Password">
									<span class="input-group-addon"><i class="icon-eye" id="showPass"></i></span>
								</div>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<!-- <div class="form-group has-feedback has-feedback-left">
								<div class="g-recaptcha" data-sitekey="6LcZL2gUAAAAAHQywruSyKJvMNL1nvERdPFSOXFH"></div>
							</div> -->

							<div class="form-group">
								<button class="btn bg-blue btn-block" name="masuk">Login <i class="icon-circle-right2 position-right"></i></button>
							</div>

							<div class="form-group login-options">
								<div class="row">
									<div class="col-sm-3">
										<label class="checkbox-inline">
											<input type="checkbox" class="styled" checked="checked">
											Remember
										</label>
									</div>

								</div>
							</div>
							<span class="help-block text-center no-margin">By continuing, you're confirming that you've read our <a href="#">Terms &amp; Conditions</a> and <a href="#">Cookie Policy</a></span>
						</div>
					</form>	 	
					<!-- /advanced login -->
					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<script type="text/javascript">
          $('#showPass').click(function() {
			if ($(this).hasClass('icon-eye')) {
				$('#loginPassword').attr('type', 'text');
				$(this).removeClass('icon-eye');
				$(this).addClass('icon-eye-blocked');
			} else {
				$('#loginPassword').attr('type', 'password');
				$(this).removeClass('icon-eye-blocked');
				$(this).addClass('icon-eye');
			}
		});
      </script>
</body>
</html>
