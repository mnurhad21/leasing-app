<?php
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
 session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

	$cashflow = $log->getCashflow();
	$leasing = $log->getLeasing();
	$nasabah = $log->getNasabah();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/picker_date.js"></script>

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/adminleasing/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Konfirmasi</span> - Order By Leasing / <?php echo $_SESSION["PRSH_NAMA"]; ?></h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Order</a></li>
							<li class="active">Konfirmasi Order</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					
					<form action="" method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h4>Dealer : CV. Mitra Angkasa Jaya Utama</h4>
								<h5 class="panel-title">Konfirmasi Order</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">

								<div class="form-group">
									<div class="col-lg-8">
										<select class="select" name="status">
											<optgroup label="Pilih Status">
												<option value="-">PILIH STATUS</option>
												<option value="SURVEY">Proses Survey</option>
												<option value="ON_KREDIT">Pemenuhan Syarat Kredit</option>
												<option value="ON_APPROVAL">Persetujuan Kredit</option>
												<option value="ON_VERIFIK">Verifikasi Konsumen</option>
												<option value="ON_ORDER">Pencairan Order</option>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-lg-4">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar"></i></span>
											<input type="text" id="from" class="form-control" name="tanggalAwal" value="<?php echo date('Y-m-d'); ?>" placeholder="Pilih Tanggal Awal&hellip;">
										</div>
									</div>
									<div class="col-lg-4">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
												<input type="text" id="to" class="form-control" name="tanggalAkhir" value="<?php echo date('Y-m-d'); ?>" placeholder="Pilih Tanggal Akhir&hellip;">
											</div>
									</div>
								</div>

								<div class="text-left">
									<button name="submit" class="btn btn-primary">Search <i class="icon-search4 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Scrollable datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Konfirmasi Order</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>
						<?php if(isset($_POST['submit'])){
						    $confirmId['status'] = $_POST['status'];
						    $confirmId['tanggalAwal'] = $_POST['tanggalAwal'];
						    $confirmId['tanggalAkhir'] = $_POST['tanggalAkhir'];
						    $ctrl = $log->getConfirmOrderByLeasing($confirmId);
						 ?>
						<table id="example" class="table datatable" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Tanggal</th>
									<!-- <th class="text-center">No ID</th> -->
									<!-- <th>Customer ID</th> -->
									<th>Nama</th>
									<th>No Telp</th>
									<th class="text-center">Alamat</th>
									<th>Type Penjualan</th>
									<!-- <th class="text-center">Leasing ID</th> -->
									<th class="text-center">Nama Leasing</th>
									<!-- <th>Unit ID</th> -->
									<th>Type</th>
									<th>Warna</th>
									<th>No Mesin</th>
									<th>No Rangka</th>
									<th>Harga Unit</th>
									<th>DP</th>
									<th>Piutang</th>
									<th>OD</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php
								 $no = 1;
								 if(is_array($ctrl) || is_object($ctrl)) {
								 foreach($ctrl as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $log->TanggalIndo($row["TP_TANGGAL"]); ?></td>
									<td><?= $row["TP_NAMACUST"]; ?></td>
									<td><?= $row["TP_NOHP"]; ?></td>
									<td><?= $row["TP_ALAMAT"]; ?></td>
									<td><?= $row["TP_TYPE_PENJUALAN"]; ?></td>
									<td class="text-center"><?= $row["TL_PRSH_NAMA"]; ?></td>
									<td><?= $row["TP_TYPE_NAMA"]; ?></td>
									<td><?= $row["TP_WARNA"]; ?></td>
									<td><?= $row["TP_NO_MESIN"]; ?></td>
									<td><?= $row["TP_NO_RANGKA"]; ?></td>
									<td>Rp. <?= number_format($row["TP_HARGA"],2,',','.'); ?></td>
									<td>Rp. <?= number_format($row["TP_DP"],2,',','.'); ?></td>
									<td>Rp. <?= number_format($row["TP_HASIL"],2,',','.'); ?></td>
									<td><?php $booking    =new DateTime($row['TP_SYS_CREATED']);
				                        $today        =new DateTime();
				                        $diff = $today->diff($booking);
				                        echo $diff->d; echo " Hari";
				                    ?></td>
									<td>
										
										<form action="status-order" method="post">
										<input type="hidden" name="id" id="id" value="<?php echo $row["TP_NOID"]; ?>">
										<select class="select" name="status">
											<optgroup label="Pilih Status">
												<option value="SURVEY"<?php if($row["TP_STATUS"] == "SURVEY"){echo "selected";} ?>>Proses Survey</option>
												<option value="ON_KREDIT" <?php if($row["TP_STATUS"] == "ON_KREDIT"){echo "selected";} ?>>Pemenuhan Syarat Kredit</option>
												<option value="ON_APPROVAL" <?php if($row["TP_STATUS"] == "ON_APPROVAL"){echo "selected";} ?>>Persetujuan Kredit</option>
												<option value="ON_VERIFIK" <?php if($row["TP_STATUS"] == "ON_VERIFIK"){echo "selected";} ?>>Verifikasi Konsumen</option>
												<option value="ON_ORDER" <?php if($row["TP_STATUS"] == "ON_ORDER"){echo "selected";} ?>>Pencairan Order</option>
											</optgroup>
										</select>
											<input type="text" class="form" name="keterangan">	
											<button name="set-link" class="btn btn-warning"><span class="icon-move-right"></span></button>
										</form>
									</td>
								</tr>
								<?php } }?>
							</tbody>
						</table>
						<?php } ?>
					</div>
					<!-- /scrollable datatable -->
					 


					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<script type="text/javascript">
	    jQuery(document).ready(function($){
	        $('.status-link').on('click',function(){
	            var getLink = $(this).attr('href');
	            var id = $("#id").val();
	            swal({
	                    title: 'Apakah Anda Yakin merubah status order?',
	                    text: 'Ubah Status Order',
	                    type: 'warning',
	                    html: true,
	                    confirmButtonColor: '#d9534f',
	                    showCancelButton: true,
	                    },function(){
	                        //createOverlay("Mohon Tunggu...");
	                         $.ajax({  
						        type  : "POST",
						        url   : "alokasi-status",
						        data  : "id=" + id,
						        success : function(result) { 
						          gOverlay.hide();
						          var data = JSON.parse(result);

						          if(data["STATUS"] == "SUCCESS") {            
						            setTimeout(function(){ 
						              window.location = "alokasi-piutang";
						            }, 300);              
						          }
						          else {
						            //sweetAlert("Pesan Kesalahan", data["MESSAGE"], "error");
						            //toastr.error(data["MESSAGE"]);
						            swal({
						              title: "GAGAL",
						              text: data["MESSAGE"],
						              type: "error",
						              showCancelButton: false,
						              confirmButtonColor: "#DD6B55",
						              confirmButtonText: "OK",
						              closeOnConfirm: false,
						              html: true
						            },
						            function(){
						              setTimeout(function(){ 
						                window.location = "alokasi-piutang";
						              }, 500);              
						            });
						          }
						        },
						        error : function(error) {   
						          gOverlay.hide();
						          alert("Gangguan pada server/jaringan\r\n" + error);
						        }
						      }); 
	                    	window.location.href = getLink
	                });
	            return false;
	        });
	    });
    </script>
	<script type="text/javascript">
     $(function(){
        $("#to").datepicker({ dateFormat: 'yy-mm-dd' });
        $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("yy-mm-dd", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to").datepicker( "option", "minDate", minValue );
        })
    });
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
		    $('#example').DataTable( {
		        "scrollY": 200,
		        "scrollX": true
		    } );
		} );
	</script>
   
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

