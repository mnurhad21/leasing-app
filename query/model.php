<?php
/**
 * The font metrics class
 *
 * Global function system app leasing
 * 
 * powered by candev-hadi
 * mail to saya-hadi@can.web.id
 **/

header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
/** GLOBAL FUNCTION **/
require_once 'config.php';
require_once 'security.php';

class model extends Security {

	function __construct() {
		date_default_timezone_set("Asia/Jakarta");
    }

    //fungsi global
    function TanggalIndo($date){
		$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 
		$tahun = substr($date, 0, 4);
		$bulan = substr($date, 5, 2);
		$tgl   = substr($date, 8, 2);
	 
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;		
		return($result);
	}

	function frmDate($date,$code){
        $explode = explode("-",$date);
        $year  = $explode[0];
        $month = (substr($explode[1],0,1)=="0")?str_replace("0","",$explode[1]):$explode[1];
        $dated = $explode[2];
        $explode_time = explode(" ",$dated);
        $dates = $explode_time[0];
        switch($code){
            // Per Object
            case 4: $format = $dates; break;                                                   
            case 5: $format = $month; break;                                                       
            case 6: $format = $year; break;               
        }       
        return $format; 
    }


	function dateRange($start,$end){
        $xdate    = $start;
        $ydate    = $end;
        $xmonth    = $start;
        $ymonth    = $end;
        $xyear    = $start;
        $yyear    = $end;
        // Jika Input tanggal berada ditahun yang sama
        if($xyear==$yyear){
            // Jika Input tanggal berada dibulan yang sama
            if($xmonth==$ymonth){
                $nday=$ydate+1-$xdate;
            } else {
                $r2=NULL;
                $nmonth = $ymonth-$xmonth;           
                $r1 = nmonth($xmonth)-$xdate+1;
                for($i=$xmonth+1;$i<$ymonth;$i++){
                    $r2 = $r2+nmonth($i);
                }
                $r3 = $ydate;
                $nday = $r1+$r2+$r3;
            }
        } else {
            // Jika Input tahun awal berbeda dengan tahun akhir
            $r2=NULL; $r3=NULL;
            $r1=nmonth($xmonth)-$xdate+1;

            for($i=$xmonth+1;$i<13;$i++){
                $r2 = $r2+nmonth($i);
            }
            for($i=1;$i<$ymonth;$i++){
                $r3 = $r3+nmonth($i);
            }
            $r4 = $ydate;
            $nday = $r1+$r2+$r3+$r4;
        }           
        return $nday;
    }


	//end

    function mobileLogin($data) {
		$loginEmail = mysqli_real_escape_string($this->conn(), $data['loginEmail']); 
    	$loginPassword = $data['loginPassword']; 

		$ip = $_SERVER['REMOTE_ADDR'];
		$browser = $_SERVER['HTTP_USER_AGENT'];
			//logikan if else rule group
		$user = $this->query("SELECT * FROM tl_user WHERE (U_ID = '$loginEmail' OR U_EMAIL = '$loginEmail') AND U_PASSWORD_HASH = '$loginPassword' AND U_STATUS = 'USER_ACTIVE'"); 

	    if (mysqli_num_rows($user) > 0){ 
	    	//session_start(); 
	        //$users = mysqli_fetch_assoc($this->conn(), $user); 
	        $users = $user->fetch_assoc();
	        $userId = $users["U_ID"]; 
	        //generate login token 
	        $dateNow = date("Y-m-d H:i:s"); 
	        $loginToken = substr(md5($users["U_FULLNAME"].$dateNow), 0, 30); 
	        $updateData = $this->query("UPDATE tl_user SET U_LOGIN_TOKEN = '$loginToken', U_LOGIN_WAKTU = '$dateNow', U_DEFAULT_LOGIN = '$browser', U_IP_POSITION = '$ip' WHERE U_ID = '$userId'"); 
	        $_SESSION["U_ID"] = $users["U_ID"]; 
	        $_SESSION["U_FULLNAME"] = $users["U_FULLNAME"]; 
	        $_SESSION["U_EMAIL"] = $users["U_EMAIL"]; 
	        $_SESSION["U_TELPON"] = $users["U_TELPON"]; 
	        $_SESSION["U_GROUP_RULE"] = $users["U_GROUP_RULE"]; 
	        $_SESSION["U_LOGIN_TOKEN"] = $users["U_LOGIN_TOKEN"]; 
	        $_SESSION["U_LOGIN_WAKTU"] = $users["U_LOGIN_WAKTU"]; 
	        $_SESSION["U_PRSH_ID"] = $users["U_PRSH_ID"]; 
	        $_SESSION["IMG_AVATAR"] = $users["U_AVATAR"];
	        $_SESSION["U_BACKGROUND"] = $users["U_BACKGROUND"];
	      // atur masuk kepage berdasarkan rule 
	 
		      if($users["U_GROUP_RULE"] == "GR_ADMIN_DEALER") { 
		        echo "<script>location.replace('dashboard-adm-dealer')</script>";
		      } elseif($users["U_GROUP_RULE"] == "GR_ADMIN_HEAD") { 
		        echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Admin Head') 
		        location.replace('dashboard-adm-oh')</script>";
		      } elseif($users["U_GROUP_RULE"] == "GR_ADMIN_LEASING") {
		      	$sessId = $_SESSION["U_PRSH_ID"];
		      	$query = $this->query("SELECT * FROM  tl_leasing WHERE TL_PRSHID = '$sessId'");
		      	$dataPrshh = mysqli_fetch_assoc($query);
		      	$_SESSION["PRSH_NAMA"] = $dataPrshh["TL_PRSH_NAMA"];
		      	echo "<script>alert('Selamat datang ".$users["U_FULLNAME"].",anda login sebagai Admin Leasing') 
		        location.replace('dashboard-adm-leasing')</script>";
		      } else { 
		        echo "<script>alert('Login Failed,')</script>"; 
		      } 
	   } else {
	    	 echo "<script>alert('Periksa lagi data login anda')</script>"; 
	    }
	}


	function mobileLogout() {
		session_start(); 
		$dateOut = date("Y-m-d H:i:s"); 
		$userId = $_SESSION["U_ID"]; 
		$name = $_SESSION["U_FULLNAME"];
		$logout = $this->query("UPDATE tl_user SET  U_LOGOUT_WAKTU = '$dateOut' WHERE U_ID = '$userId'"); 

		session_destroy(); 
		setcookie('notifLogin','Berhasil Logout',time() + 10); 
		if($logout) {
			echo "<script>alert('Anda berhasil logout ".$name."') 
			location.replace('login')</script>";
		}
	}

	function accountSetting($data1) {
		//session_start();
		$userId = mysqli_escape_string($this->conn(), $data1["userId"]);
		$name = mysqli_escape_string($this->conn(), $data1["name"]);
		$email = mysqli_escape_string($this->conn(), $data1["email"]);
		$password = mysqli_escape_string($this->conn(), $data1["password"]);
		$avatar = mysqli_escape_string($this->conn(), $data1["IMG_PATH"]);

		$target_dir = "./layouts/asset/assets/images/profile/";

	    $newname = date('dmYHis').str_replace(" ", "", basename($_FILES["IMG_PATH"]['name']));
	    $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["IMG_PATH"]['name']));
	    $target_file = $target_dir . $newfilename; 
	    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		move_uploaded_file($_FILES["IMG_PATH"]["tmp_name"], $target_file);
		// $ddd = "UPDATE tl_user SET U_FULLNAME = '$name', U_EMAIL = '$email', U_PASSWORD_HASH = '$password', 	U_AVATAR = '$newname' WHERE U_ID = '$userId'";
		// echo "tt". $ddd;

    	$query = $this->query("UPDATE tl_user SET U_FULLNAME = '$name', U_EMAIL = '$email', U_PASSWORD_HASH = '$password', 	U_AVATAR = '$newname' WHERE U_ID = '$userId'"); 

		if($query){
			echo "<script>alert('Profile Berhasil diupdate')
			location.replace('setting-account')</script></script>";
		} else {
			echo "<script>alert('Failed,update profil gagal')</script>";
		}
	}

	function updateBack($back) {
		$id = mysqli_escape_string($this->conn(), $back["userId"]);
		$background = mysqli_escape_string($this->conn(), $back["IMG_PATH"]);
		$target_dir = "./layouts/asset/assets/images/back/";

	    $newname = date('dmYHis').str_replace(" ", "", basename($_FILES["IMG_PATH"]['name']));
	    $newfilename = date('dmYHis').str_replace(" ", "", basename($_FILES["IMG_PATH"]['name']));
	    $target_file = $target_dir . $newfilename; 
	    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

		move_uploaded_file($_FILES["IMG_PATH"]["tmp_name"], $target_file);
		// $ddd = "UPDATE tl_user SET U_FULLNAME = '$name', U_EMAIL = '$email', U_PASSWORD_HASH = '$password', 	U_AVATAR = '$newname' WHERE U_ID = '$userId'";
		// echo "tt". $ddd;

    	$query1 = $this->query("UPDATE tl_user SET U_BACKGROUND = '$newname' WHERE U_ID = '$id'"); 

		if($query1){
			echo "<script>alert('Background Berhasil diupdate')
			location.replace('my-profile')</script></script>";
		} else {
			echo "<script>alert('Failed,update profil gagal')</script>";
		}
	}

	//==== UNIT STOCK =====//

	function tambahUnit($unit) {
		//session_start();
		$unitId = mysqli_escape_string($this->conn(), $unit["unitId"]);
		$unitType = mysqli_escape_string($this->conn(), $unit["unitType"]);
		$unitWarna = mysqli_escape_string($this->conn(), $unit["unitWarna"]);
		$unitRangka = mysqli_escape_string($this->conn(), $unit["unitRangka"]);
		$unitMesin = mysqli_escape_string($this->conn(), $unit["unitMesin"]);
		$unitTahun = mysqli_escape_string($this->conn(), $unit["unitTahun"]);
		$unitHarga = mysqli_escape_string($this->conn(), $unit["unitHarga"]);
		$user_id = $_SESSION["U_ID"];
		$datetime = date("Y-m-d H:i:s");
		$date = date("Y-m-d");
		$date1 = date("Y");

		$unitStock = $this->query("INSERT INTO tl_unit (TU_UNITID, TU_TYPE, TU_WARNA, TU_NOMER_RANGKA, TU_NOMER_MESIN, TU_HARGA, TU_TAHUN, TU_CREATED_USER, 	TU_STATUS, TU_TGL_WAREHOUSE, TU_SYS_CREATED) VALUES ('$unitId', '$unitType', '$unitWarna', '$unitRangka', '$unitMesin', '$unitHarga', '$date1', '$user_id', 'READY', '$date', '$datetime')");

		if($unitStock) {
			echo "<script>alert('Unit Stock Berhasil ditambahkan')
			location.replace('stock-unit')</script></script>";
		} else {
			echo "<script>alert('tambah Unit Stock gagal')
			location.replace('stock-unit')</script></script>";
		}
	}

	//data unit
	function addUnit($unit) {
		$unitType = mysqli_escape_string($this->conn(), $unit["unitType"]);
		$unitHarga = mysqli_escape_string($this->conn(), $unit["unitHarga"]);
		$angsur = str_replace(".", "", $unitHarga);

		$unitData = $this->query("INSERT INTO tl_unit_data (TUD_NAMA, TUD_HARGA) VALUES ('$unitType', '$angsur')");

		if($unitData) {
			echo "<script>alert('Unit Stock Berhasil ditambahkan')</script>";
		} else {
			echo "<script>alert('tambah Unit Stock gagal')";
		}
	}

	function getDataUnit() {
		$rows = NULL;
    	$query = $this->query("SELECT * FROM tl_unit_data ORDER BY TUD_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	function dataUnitId($unitId) {
		$query = $this->query("SELECT * FROM tl_unit_data WHERE TUD_ID = '$unitId'");
		$row = $query->fetch_assoc();
		return $row;
	}

	function editDataUnit($unit) {
		$unitId = mysqli_escape_string($this->conn(), $unit['unitId']);
		$type = $this->clean_all($unit['unitType']);
		$harga = $this->clean_all($unit['unitHarga']);

		$query = $this->query("UPDATE tl_unit_data SET TUD_NAMA = '$type', TUD_HARGA = '$harga' WHERE TUD_ID = '$unitId'");

		if($query) {
			echo "<script>alert('Stock Unit berhasil diupdate')
			location.replace('unit-first')</script></script>";
		} else {
			echo "<script>alert('Driver gagal diupdate')</script>";
		}
	}

	function deleteDataUnit($unitId) {
		$query = $this->query("DELETE FROM tl_unit_data WHERE TUD_ID = '$unitId'");
		if($query) {
			echo "<script>alert('Unit Stock berhasil dihapus')
			location.replace('unit-first')</script>";
		} else {
			echo "<script>alert('Unit Stock gagal dihapus')</script>";
		}
	}


	function getUnit() {
		$rows = NULL;
    	$query = $this->query("SELECT * FROM tl_unit ORDER BY TU_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	//get unit by status
	function getUnitStatus() {
		$rows = NULL;
    	$query = $this->query("SELECT * FROM tl_unit WHERE TU_STATUS = 'READY' ORDER BY TU_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	function get_unit_id($unitId) {
		
		$query = $this->query("SELECT * FROM tl_unit where TU_UNITID = '$unitId'");
		$row = $query->fetch_assoc(); 
	    return $row; 
	}

	function editUnit($unit) {
		$unit_id 				= mysqli_escape_string($this->conn(), $unit['unitId']);
    	$type 					= $this->clean_all($unit['unitType']);
		$warna 					= $this->clean_all($unit['unitWarna']);
		$nomerRangka					= $this->clean_all($unit['unitRangka']); 
		$nomerMesin 				= $this->clean_all($unit['unitMesin']);
		$status					= $this->clean_all($unit['unitStatus']); 
		$harga = $this->clean_all($unit['unitHarga']);
		$tahun = $this->clean_all($unit['unitTahun']);  
		$date = date("Y-m-d H:i:s");

    	$query = $this->query("UPDATE tl_unit SET TU_TYPE = '$type', TU_WARNA ='$warna', TU_NOMER_RANGKA = '$nomerRangka', 	TU_NOMER_MESIN = '$nomerMesin', TU_STATUS = '$status', TU_TAHUN = '$tahun', TU_HARGA = '$harga', TU_SYS_UPDATED = '$date' WHERE TU_UNITID = '$unit_id'");

		if($query){
			echo "<script>alert('Stock Unit berhasil diupdate')
			location.replace('stock-unit')</script></script>";
		} else {
			echo "<script>alert('Driver gagal diupdate')</script>";
		}
	}

	function deleteUnit($unitId) {
		$query = $this->query("DELETE FROM tl_unit WHERE TU_UNITID = '$unitId'");
		if($query){
			echo "<script>alert('Unit Stock berhasil dihapus')
			location.replace('stock-unit')</script>";
		} else {
			echo "<script>alert('Unit Stock gagal dihapus')</script>";
		}
	}

	//==== END ====//


	//=== CASHFLOW ===// str_replace(".", "", $jmlNominal)

	function tambahCashflow($cashflow) {
		$tanggal = mysqli_escape_string($this->conn(), $cashflow["tanggal"]);
		$tgl = date("Y-m-d", strtotime($tanggal));
		$noId = mysqli_escape_string($this->conn(), $cashflow["noId"]);
		$namaBank = mysqli_escape_string($this->conn(), $cashflow["namaBank"]);
		// $namaAkun = mysqli_escape_string($this->conn(), $cashflow["namaAkun"]);
		$terimaDari = mysqli_escape_string($this->conn(), $cashflow["terimaDari"]);
		$nominal = mysqli_escape_string($this->conn(), $cashflow["totalNominal"]);
		$totalNominal = str_replace(".", "", $nominal);
		$keterangan = mysqli_escape_string($this->conn(), $cashflow["keterangan"]);
		$user_id = $_SESSION["U_ID"];
		$date = date("Y-m-d H:i:s");

		$leasing = $this->query("SELECT * FROM tl_leasing WHERE TL_PRSHID = '$terimaDari'");
		$piutang = $leasing->fetch_assoc();
		$nominal = $piutang["TL_NOMINAL_PIUTANG"];

		//hitung 
		$hasil = $nominal - $totalNominal;
		// print_r($totalNominal);
		// exit();

		$cash = $this->query("INSERT INTO tl_cashflow (TC_TANGGAL, TC_TRANS_ID, TC_NAMA_BANK, TC_NAMA_AKUN, TC_TRANS_FROM, TC_NOMINAL, TC_STATUS, TC_KETERANGAN, TC_CREATED_USER, 	TC_SYS_CREATED) VALUES ('$tgl', '$noId', '$namaBank', '-', '$terimaDari', '$totalNominal', 'ON_CHECKING', '$keterangan', '$user_id', '$date')");

		$query2 = $this->query("UPDATE tl_penjualan SET TP_CASH_STATUS = 'ON_CHECKING' WHERE TP_CHECK = 'Y' AND TP_LEASING_ID = '$terimaDari'");

		if($cash) {
			$query1 = $this->query("UPDATE tl_leasing SET TL_NOMINAL_PIUTANG = '$hasil' WHERE TL_PRSHID = '$terimaDari'");
			echo "<script>alert('Cashflow Berhasil ditambahkan')
			location.replace('cashflow')</script></script>";
		} else {
			echo "<script>alert('Cashflow gagal ditambahkan')
			location.replace('cashflow')</script></script>";
		}
	}

	function getCashflow() {
		$rows = NULL;
    	$query = $this->query("SELECT A.*, B.* FROM tl_cashflow AS A INNER JOIN tl_leasing AS B ON A.TC_TRANS_FROM = B.TL_PRSHID ORDER BY A.TC_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	function get_cashflow_id($cashflowId) {
		
		$query = $this->query("SELECT * FROM tl_cashflow where TC_TRANS_ID = '$cashflowId'");
		$row = $query->fetch_assoc(); 
	    return $row; 
	}

	function editCashflow($cashflowEdit) {
		$tanggal = mysqli_escape_string($this->conn(), $cashflowEdit["tanggal"]);
		$tgl = date("Y-m-d", strtotime($tanggal));
		$noId = mysqli_escape_string($this->conn(), $cashflowEdit["noId"]);
		$namaBank = mysqli_escape_string($this->conn(), $cashflowEdit["namaBank"]);
		$namaAkun = mysqli_escape_string($this->conn(), $cashflowEdit["namaAkun"]);
		$terimaDari = mysqli_escape_string($this->conn(), $cashflowEdit["terimaDari"]);
		$nominal = mysqli_escape_string($this->conn(), $cashflowEdit["totalNominal"]);
		$totalNominal = str_replace(".", "", $nominal);
		$keterangan = mysqli_escape_string($this->conn(), $cashflowEdit["keterangan"]);
		$status = mysqli_escape_string($this->conn(), $cashflowEdit["cashflowStatus"]);
		$date = date("Y-m-d H:i:s");

    	$query = $this->query("UPDATE tl_cashflow SET TC_TANGGAL = '$tgl', TC_NAMA_BANK ='$namaBank', TC_NAMA_AKUN = '$namaAkun', TC_TRANS_FROM = '$terimaDari', TC_NOMINAL = '$totalNominal', TC_STATUS = '$status', TC_KETERANGAN = '$keterangan', TC_SYS_UPDATED = '$date' WHERE TC_TRANS_ID = '$noId'");

		if($query){
			echo "<script>alert('Cashflow berhasil diupdate')
			location.replace('cashflow')</script></script>";
		} else {
			echo "<script>alert('Cashflow gagal diupdate')</script>";
		}
	}

	function deleteCashflow($cashflowId) {
		$query = $this->query("DELETE FROM tl_cashflow WHERE TC_TRANS_ID = '$cashflowId'");
		if($query){
			echo "<script>alert('Cashflow berhasil dihapus')
			location.replace('cashflow')</script>";
		} else {
			echo "<script>alert('Cashflow gagal dihapus')</script>";
		}
	}

	//=== NASABAH ===//

	function tambahNasabah($nasabah) {
		$custId = mysqli_escape_string($this->conn(), $nasabah["customerId"]);
		$identiId = mysqli_escape_string($this->conn(), $nasabah["identitasId"]);
		$panggilan = mysqli_escape_string($this->conn(), $nasabah["panggilan"]);
		$nama = mysqli_escape_string($this->conn(), $nasabah["nama"]);
		$gender = mysqli_escape_string($this->conn(), $nasabah["gender"]);
		$noTelp = mysqli_escape_string($this->conn(), $nasabah["noTelp"]);
		$alamat = mysqli_escape_string($this->conn(), $nasabah["alamat"]);
		$user_id = $_SESSION["U_ID"];
		$date = date("Y-m-d H:i:s");

		$query = $this->query("INSERT INTO  tl_nasabah (TN_CARDID, TN_PANGGILAN, TN_CUSTID, TN_NAMA, TN_TELPON, TN_ALAMAT, TN_JK, TN_CREATED_USER, TN_SYS_CREATED) VALUES ('$identiId', '$panggilan', '$custId', '$nama', '$noTelp', '$alamat', '$gender', '$user_id', '$date')");

		if($query){
			echo "<script>alert('Nasabah berhasil ditambahkan')
			location.replace('list-nasabah')</script>";
		} else {
			echo "<script>alert('Nasabah gagal ditambahkan')</script>";
		}

	}

	function getNasabah() {
		$rows = NULL;
    	$query = $this->query("SELECT * FROM tl_nasabah ORDER BY TN_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	function getCashflowId() {
		$rows = NULL;
		$query = $this->query("SELECT * FROM tl_cashflow WHERE TC_STATUS = 'ON_CHECKING' ORDER BY TC_SYS_CREATED DESC");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function getNasabahId($nasabahId) {
		$query = $this->query("SELECT * FROM tl_nasabah where TN_CUSTID = '$nasabahId'");
		$row = $query->fetch_assoc(); 
	    return $row; 
	}

	function updateNasabah($dataNasabah) {
		$custId = mysqli_escape_string($this->conn(), $dataNasabah["customerId"]);
		$identiId = mysqli_escape_string($this->conn(), $dataNasabah["identitasId"]);
		$panggilan = mysqli_escape_string($this->conn(), $dataNasabah["panggilan"]);
		$nama = mysqli_escape_string($this->conn(), $dataNasabah["nama"]);
		$gender = mysqli_escape_string($this->conn(), $dataNasabah["gender"]);
		$noTelp = mysqli_escape_string($this->conn(), $dataNasabah["noTelp"]);
		$alamat = mysqli_escape_string($this->conn(), $dataNasabah["alamat"]);
		$date = date("Y-m-d H:i:s");

		$query = $this->query("UPDATE tl_nasabah SET TN_CARDID = '$identiId', TN_PANGGILAN = '$panggilan', TN_NAMA = '$nama', TN_TELPON = '$noTelp', TN_ALAMAT = '$alamat', TN_JK = '$gender', TN_SYS_UPDATES = '$date' WHERE TN_CUSTID = '$custId'");

		if($query){
			echo "<script>alert('Customer berhasil dirubah')
			location.replace('list-nasabah')</script>";
		} else {
			echo "<script>alert('Customer gagal dirubah')</script>";
		}
	}

	function deleteNasabah($id) {
	   $query = $this->query("DELETE FROM tl_nasabah WHERE TN_CUSTID = '$id'");
		if($query){
			echo "<script>alert('Customer berhasil dihapus')
			location.replace('list-nasabah')</script>";
		} else {
			echo "<script>alert('Customer gagal dihapus')</script>";
		}	
	}

	//=== LEASING ====//
	function tambahLeasing($leasing) {
		$prshId = mysqli_escape_string($this->conn(), $leasing["leasingId"]);
		$nama = mysqli_escape_string($this->conn(), $leasing["namaLeasing"]);
		$noTelp = mysqli_escape_string($this->conn(), $leasing["noTelp"]);
		// $jumlahPiutang = mysqli_escape_string($this->conn(), $leasing["jumlahPiutang"]);
		// $totalNominal = mysqli_escape_string($this->conn(), $leasing["totalNominal"]);
		// $nominal = str_replace(".", "", $totalNominal);
		$alamat = mysqli_escape_string($this->conn(), $leasing["alamat"]);
		$user_id = $_SESSION["U_ID"];
		$date = date("Y-m-d H:i:s");

		$query = $this->query("INSERT INTO  tl_leasing (TL_PRSHID, TL_PRSH_NAMA, TL_ALAMAT, TL_TELPON, 	TL_STATUS, TL_JML_PIUTANG, TL_NOMINAL_PIUTANG, TL_CREATED_USER, TL_SYS_CREATED) VALUES ('$prshId', '$nama', '$alamat', '$noTelp', 'AKTIF', '0', '0', '$user_id', '$date')");

		if($query){
			echo "<script>alert('leasing berhasil ditambahkan')
			location.replace('leasing')</script>";
		} else {
			echo "<script>alert('leasing gagal ditambahkan')</script>";
		}

	}

	function getLeasing() {
		$rows = NULL;
    	$query = $this->query("SELECT * FROM tl_leasing ORDER BY TL_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	function getLeasingId($leasingId) {
		$query = $this->query("SELECT * FROM tl_leasing where TL_PRSHID = '$leasingId'");
		$row = $query->fetch_assoc(); 
	    return $row; 
	}

	function updateLeasing($dataUpdate) {
		$prshId = mysqli_escape_string($this->conn(), $dataUpdate["leasingId"]);
		$nama = mysqli_escape_string($this->conn(), $dataUpdate["namaLeasing"]);
		$noTelp = mysqli_escape_string($this->conn(), $dataUpdate["noTelp"]);
		// $jumlahPiutang = mysqli_escape_string($this->conn(), $dataUpdate["jumlahPiutang"]);
		// $totalNominal = mysqli_escape_string($this->conn(), $dataUpdate["totalNominal"]);
		// $nominal = str_replace(".", "", $totalNominal);
		$alamat = mysqli_escape_string($this->conn(), $dataUpdate["alamat"]);
		$status = mysqli_escape_string($this->conn(), $dataUpdate["status"]);
		$date = date("Y-m-d H:i:s");

		$query = $this->query("UPDATE tl_leasing SET TL_PRSH_NAMA = '$nama', TL_ALAMAT = '$alamat', TL_TELPON = '$noTelp', 	TL_STATUS = '$status', TL_JML_PIUTANG = '0', 	TL_NOMINAL_PIUTANG = '0' WHERE TL_PRSHID = '$prshId'");

		if($query){
			echo "<script>alert('leasing berhasil diperbarui')
			location.replace('leasing')</script>";
		} else {
			echo "<script>alert('leasing gagal diperbarui')</script>";
		}
	}

	function deleteLeasing($leasing_id) {
		$query = $this->query("DELETE FROM tl_leasing WHERE TL_PRSHID = '$leasing_id'");
		if($query){
			echo "<script>alert('leasing berhasil dihapus')
			location.replace('leasing')</script>";
		} else {
			echo "<script>alert('leasing gagal dihapus')</script>";
		}	
	}
	//manajemen user
	function tambahUser($dataUser) {
		$username = mysqli_escape_string($this->conn(), $dataUser["username"]);
		$nama = mysqli_escape_string($this->conn(), $dataUser["nama"]);
		$email = mysqli_escape_string($this->conn(), $dataUser["email"]);
		$noTelp = mysqli_escape_string($this->conn(), $dataUser["noTelp"]);
		$rule = mysqli_escape_string($this->conn(), $dataUser["rule"]);
		$leasingId = mysqli_escape_string($this->conn(), $dataUser["leasingId"]);
		$password = md5('12345678');
		$status = 'USER_ACTIVE';
		$user_id = $_SESSION["U_ID"];
		$date = date("Y-m-d H:i:s");
		$enkrip = base64_encode($user_id.$date);

		$query = $this->query("INSERT INTO tl_user (U_ID, U_FULLNAME, U_EMAIL, U_TELPON, U_PASSWORD_HASH, U_GROUP_RULE, U_STATUS, U_PRSH_ID, U_CREATED_USER, U_KEY, U_SYS_CREATED) VALUES ('$username', '$nama', '$email', '$noTelp', '$password', '$rule', '$status', '$leasingId', '$user_id', '$enkrip', '$date')");

		if($query){
			echo "<script>alert('Data User berhasil ditambahkan,dengan password default')
			location.replace('management-user')</script>";
		} else {
			echo "<script>alert('Data user gagal ditambahkan')</script>";
		}

	}

	function getUser() {
		$user_id = $_SESSION["U_ID"];
		$rows = NULL;
    	$query = $this->query("SELECT * FROM tl_user WHERE U_ID != '$user_id' ORDER BY U_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	function getUserById($key) {
		$query = $this->query("SELECT * FROM tl_user WHERE U_KEY = '$key'");
		$row = $query->fetch_assoc(); 
	    return $row; 
	}

	function updateUser($userData) {
		$username = mysqli_escape_string($this->conn(), $userData["username"]);
		$nama = mysqli_escape_string($this->conn(), $userData["nama"]);
		$email = mysqli_escape_string($this->conn(), $userData["email"]);
		$noTelp = mysqli_escape_string($this->conn(), $userData["noTelp"]);
		$rule = mysqli_escape_string($this->conn(), $userData["rule"]);
		$status = mysqli_escape_string($this->conn(), $userData["status"]);

		$query = $this->query("UPDATE tl_user SET U_FULLNAME = '$nama', U_EMAIL = '$email', U_TELPON = '$noTelp', U_GROUP_RULE = '$rule', U_STATUS = '$status' WHERE U_ID = '$username'");
		if($query){
			echo "<script>alert('Data User berhasil diperbarui')
			location.replace('management-user')</script>";
		} else {
			echo "<script>alert('Data user gagal diperbarui')</script>";
		}

	}

	function deleteUser($key) {
		$query = $this->query("DELETE FROM tl_user WHERE U_KEY = '$key'");
		if($query){
			echo "<script>alert('User berhasil dihapus')
			location.replace('mngmt-user')</script>";
		} else {
			echo "<script>alert('User gagal dihapus')</script>";
		}	
	}

	//=== VIEW PENJUALAN/ORDER ====//
	function tambahPenjualan($jual) {
		$transId = mysqli_escape_string($this->conn(), $jual["transaksiID"]);
		$tanggal = mysqli_escape_string($this->conn(), $jual["tanggal"]);
		$tgl = date("Y-m-d", strtotime($tanggal));
		$custId = mysqli_escape_string($this->conn(), $jual["idCustomer"]);
		$nama = mysqli_escape_string($this->conn(), $jual["nama"]);
		$noTelp = mysqli_escape_string($this->conn(), $jual["noTelp"]);
		$alamat = mysqli_escape_string($this->conn(), $jual["alamat"]);
		$type = mysqli_escape_string($this->conn(), $jual["typePenjualan"]);
		$leasing = mysqli_escape_string($this->conn(), $jual["leasingId"]);
		$dp =  mysqli_escape_string($this->conn(), $jual["uangMuka"]);
		$uangMuka = str_replace(".", "", $dp);
		$tenor =  mysqli_escape_string($this->conn(), $jual["tenor"]);
		$angsuran =  mysqli_escape_string($this->conn(), $jual["angsuran"]);
		$angsur = str_replace(".", "", $angsuran);
		$typeUnit =  mysqli_escape_string($this->conn(), $jual["unitId"]);
		$unitNama = mysqli_escape_string($this->conn(), $jual["unitNama"]);
		$unitWarna = mysqli_escape_string($this->conn(), $jual["unitWarna"]);
		$unitRangka = mysqli_escape_string($this->conn(), $jual["unitRangka"]);
		$unitMesin = mysqli_escape_string($this->conn(), $jual["unitMesin"]);
		$unitHarga = mysqli_escape_string($this->conn(), $jual["unitHarga"]);
		$user_id = $_SESSION["U_ID"];
		$date = date("Y-m-d H:i:s");
		$status = 'SURVEY';

		$jmlPotong = $unitHarga - $uangMuka;
		//perhitungan alokasi 
		$alokasi = $jmlPotong / $tenor;
		$hasilAlokasi = $alokasi * 0.01;

		$query = $this->query("INSERT INTO tl_penjualan (TP_NOID, TP_TANGGAL, TP_CUST_ID, TP_NAMACUST, 	TP_ALAMAT, TP_NOHP, 	TP_TYPE_PENJUALAN, TP_LEASING_ID, TP_DP, TP_TENOR, TP_ANGSURAN, TP_UNIT_ID, TP_TYPE_NAMA, TP_WARNA, TP_NO_RANGKA, TP_NO_MESIN, TP_HARGA, TP_HASIL, TP_STATUS, TP_CASH_STATUS, TP_CHECK, TP_CREATED_USER, TP_SYS_CREATED) VALUES ('$transId', '$tgl', '$custId', '$nama', '$alamat', '$noTelp', '$type', '$leasing', '$uangMuka', '$tenor', '$angsur', '$typeUnit', '$unitNama', '$unitWarna', '$unitRangka', '$unitMesin', '$unitHarga', '$jmlPotong', '$status', 'ON_ALOKASI', 'Y', '$user_id', '$date')");
		
		//get data leasing dan update data leasing
		$getData = $this->query("SELECT * FROM tl_leasing WHERE TL_PRSHID = '$leasing'");
		$row = $getData->fetch_assoc();
		$jmlPiutang = $row["TL_JML_PIUTANG"];
		$nominal = $row["TL_NOMINAL_PIUTANG"];
		$hasil = $jmlPiutang + 1;
		$hasil1 = $nominal + $jmlPotong;
		$update = $this->query("UPDATE tl_leasing SET TL_JML_PIUTANG = '$hasil', TL_NOMINAL_PIUTANG = '$hasil1' WHERE TL_PRSHID = '$leasing'");

		//update unit stock
		$query2 = $this->query("UPDATE tl_unit SET TU_STATUS = 'ORDER', TU_SYS_UPDATED = '$date' WHERE TU_UNITID = '$typeUnit'");

		//insert ke tabel alokasi per penjualan
		$query1 = $this->query("INSERT INTO  tl_alokasi_piutang (TAP_TRANS_ID, TAP_CUSTID, TAP_NAMA, TAP_LEASING_ID, TAP_UANG_MUKA, TAP_UNITID, TAP_HARGA_ITEM, TAP_JANGKA_WAKTU, TAP_HASIL, TAP_HASIL_BUNGA, TAP_TANGGAL, TAP_SYS_CREATED) VALUES ('$transId', '$custId', '$nama', '$leasing', '$uangMuka', '$typeUnit', '$unitHarga', '$tenor', '$alokasi', '$hasilAlokasi', '$tgl', '$date')");

		if($query && $update && $query1 && $query2) {
			echo "<script>alert('Transaksi Penjualan berhasil ditambahkan')
			location.replace('view-penjualan')</script>";
		} else {
			echo "<script>alert('Transaksi Penjualan gagal ditambahkan')</script>";
		}
	}

	//=== ALOKASI PIUTANG ===//
	function getAlokasi($alokasi) {
		$custId = mysqli_escape_string($this->conn(), $_POST["idCustomer"]);
		// $custname = mysqli_escape_string($this->conn(), $_POST["namaCust"]);
		$leasingIdX = mysqli_escape_string($this->conn(), $_POST["transaksiId"]);
		$leasingId = mysqli_escape_string($this->conn(), $_POST["terimaDari"]);
		$leasingIdN = mysqli_escape_string($this->conn(), $_POST["terimaDariId"]);
		$tanggal1 = mysqli_escape_string($this->conn(), $_POST["tanggalAwal"]);
		$tanggal2 = mysqli_escape_string($this->conn(), $_POST["tanggalAkhir"]);

		if($custId === "") {
			$rows = NULL;
			$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_LEASING_ID = '$leasingIdN' AND A.TP_CASH_STATUS = 'ON_CHECKING' AND TP_CHECK = 'Y' AND A.TP_TANGGAL BETWEEN '$tanggal1' AND '$tanggal2'");
			while($row = $query->fetch_assoc()) {
	    		$rows[] = $row;
	    	}
	    	return $rows;
	    	//print_r($rows);
		} elseif($leasingId === "") {
			$rows = NULL;
			$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE (A.TP_CUST_ID = '$custId') AND A.TP_TANGGAL BETWEEN '$tanggal1' AND '$tanggal2'");
			while($row = $query->fetch_assoc()) {
	    		$rows[] = $row;
	    	}
	    	return $rows;
	    	//print_r($rows);
	    } else {
			$rows = NULL;
			$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE (A.TP_CUST_ID = '$custId') AND A.TP_LEASING_ID = '$leasingIdN' AND A.TP_CASH_STATUS = 'ON_CHECKING' AND TP_CHECK = 'Y' AND A.TP_TANGGAL BETWEEN '$tanggal1' AND '$tanggal2'");
			while($row = $query->fetch_assoc()) {
	    		$rows[] = $row;
	    	}
	    	return $rows;
	    	//print_r($rows);
		}
	}

	function ubahStatus($statusId) {
		session_start();
		$id = mysqli_escape_string($this->conn(), $statusId["id"]);
		$status = mysqli_escape_string($this->conn(), $statusId["status"]);
		$rule = $_SESSION["U_GROUP_RULE"];

		$query = $this->query("UPDATE tl_penjualan SET TP_STATUS = '$status' WHERE TP_NOID = '$id'");
		if($query){
			if($rule === "GR_ADMIN_HEAD") {
				echo "<script>alert('Status berhasil diperbarui')
				location.replace('order-list')</script>";
			} else {
				echo "<script>alert('Status berhasil diperbarui')
				location.replace('confirm-order')</script>";
			}
		} else {
			echo "<script>alert('Status gagal diperbarui')</script>";
		}	
	}

	function statusAlokasi($statusId){
		$id = mysqli_escape_string($this->conn(), $statusId["transaksiId"]);
		$penjualanId = mysqli_escape_string($this->conn(), $statusId["penjualanId"]);
		$status = 'ON_ALOKASI';

		//get data cashflow 
		$query2 = $this->query("SELECT * FROM tl_cashflow WHERE TC_TRANS_ID = '$id'");
		$ss = mysqli_fetch_assoc($query2);
		$data1 = $ss["TC_NOMINAL"];

		//GET DATA PENJUALAM
		$query3 = $this->query("SELECT SUM(TP_HASIL) AS jmlK FROM tl_penjualan WHERE TP_LEASING_ID = '$penjualanId' AND TP_CHECK = 'Y'");
		$dd = mysqli_fetch_assoc($query3);
		$data2 = $dd["jmlK"];

		if($data1 < $data2) {
			$query = $this->query("UPDATE tl_cashflow SET TC_STATUS = '$status' WHERE TC_TRANS_ID = '$id'");

			if($query) {
				$query1 = $this->query("UPDATE tl_penjualan SET TP_CASH_STATUS = 'ON_ALOKASI', TP_CHECK = 'N' WHERE TP_LEASING_ID = '$penjualanId'");
				echo "<script>alert('Status berhasil diperbarui')
					location.replace('alokasi-piutang')</script>";
			} else {
				echo "<script>alert('Status gagal diperbarui')</script>";
			}
		} else {
			echo "<script>alert('Nominal Melebihi data cashflow')
					location.replace('alokasi-piutang')</script>";
		}
	}

	//=== VIEW ORDER ===//

	function getViewOrder($sts) {
		$leasingId = $id = mysqli_escape_string($this->conn(), $sts["leasingId"]);
		$status = $id = mysqli_escape_string($this->conn(), $sts["status"]);

		$rows = NULL;
		$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_LEASING_ID = '$leasingId' AND TP_STATUS = '$status'");
		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function listOrder() {
		$rows = NULL;
		$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID ORDER BY TP_SYS_CREATED DESC");

		while($row = $query->fetch_assoc()) {
			$rows[] = $row;
		}

		return $rows;
	}

	function getConfirmOrder($confirm) {
	   $tanggal1 = mysqli_escape_string($this->conn(), $confirm["tanggalAwal"]);
	   $tanggal2 = mysqli_escape_string($this->conn(), $confirm["tanggalAkhir"]);

	   	$rows = NULL;
		$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_TANGGAL BETWEEN '$tanggal1' AND '$tanggal2'");
		while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	function getPDF($pdfId) {
	   $query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_NOID = '$pdfId'");
	   $row = $query->fetch_assoc(); 
	   return $row; 	
	}

	function getAllUser() {
		$rows = NULL;
    	$query = $this->query("SELECT * FROM tl_user WHERE U_STATUS = 'USER_ACTIVE' ORDER BY U_SYS_CREATED DESC");
    	while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}

    	return $rows;
	}

	function getSumCashflow() {
		$rows = NULL;
    	$query = $this->query("SELECT SUM(TC_NOMINAL) AS jml FROM tl_cashflow");
    	$row = $query->fetch_assoc();
    	$rows = $row["jml"];
    	return $rows;
	}

	function getSumPiutang() {
		$rows = NULL;
    	$query = $this->query("SELECT SUM(TL_NOMINAL_PIUTANG) AS jml FROM tl_leasing");
    	$row = $query->fetch_assoc();
    	$rows = $row["jml"];
    	return $rows;
	}



	function getConfirmOrderByLeasing($confirmId) {
	    $status = mysqli_escape_string($this->conn(), $confirmId["status"]);
	    $tglAwal = mysqli_escape_string($this->conn(), $confirmId["tanggalAwal"]);
	    $tglAkhir = mysqli_escape_string($this->conn(), $confirmId["tanggalAkhir"]);
	    $prshId = $_SESSION["U_PRSH_ID"];
	   	$rows = NULL;

	   	if($status == "-") {
	   		$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_LEASING_ID = '$prshId' AND A.TP_TANGGAL BETWEEN '$tglAwal' AND '$tglAkhir' ORDER BY A.TP_SYS_CREATED DESC");
	   	} else {
	   	   $query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_STATUS = '$status' AND A.TP_LEASING_ID = '$prshId' AND A.TP_TANGGAL BETWEEN '$tglAwal' AND '$tglAkhir' ORDER BY A.TP_SYS_CREATED DESC");
	   	}

		while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	function piutangLeasing() {
		$prshId = $_SESSION["U_PRSH_ID"];
		$rows = NULL;
		$query = $this->query("SELECT SUM(TP_HASIL) AS jml FROM tl_penjualan WHERE TP_LEASING_ID = '$prshId'");
		$row = $query->fetch_assoc();
		$rows = $row["jml"];
		return $rows;
	}

	function ubahStatusByLeasing($statusId) {
		session_start();
		$id = mysqli_escape_string($this->conn(), $statusId["id"]);
		$status = mysqli_escape_string($this->conn(), $statusId["status"]);
		$keterangan = mysqli_escape_string($this->conn(), $statusId["keterangan"]);
		$idUser = $_SESSION["U_ID"];
		$datetime = date("Y-m-d H:i:s");

		$query = $this->query("UPDATE tl_penjualan SET TP_STATUS = '$status', TP_KETERANGAN = '$keterangan', TP_UPDATED_USER = '$idUser',TP_SYS_UPDATED = '$datetime' WHERE TP_NOID = '$id'");
		if($query){
			echo "<script>alert('Status berhasil diperbarui')
			location.replace('list-data-order')</script>";
		} else {
			echo "<script>alert('Status gagal diperbarui')</script>";
		}	
	}

	function historyByLeasing(){
		$prshId = $_SESSION["U_PRSH_ID"];
		$user_id = $_SESSION["U_ID"];
		$rows = 0;
		$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID AND A.TP_LEASING_ID = '$prshId' AND A.TP_UPDATED_USER = '$user_id'  ORDER BY A.TP_SYS_CREATED DESC");
		while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	function getHistory() {
		$prshId = $_SESSION["U_PRSH_ID"];
		$user_id = $_SESSION["U_ID"];
		$rows = array();
		$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID AND A.TP_LEASING_ID = '$prshId' AND A.TP_UPDATED_USER = '$user_id'  ORDER BY A.TP_SYS_CREATED DESC");
		while($row = mysqli_fetch_array($query)) {
			$rows[] = $row;
		}
    	return $rows;
	}

	//=== REPORT ===//
	function getReportStock($unit) {
		 $tanggal1 = mysqli_escape_string($this->conn(), $unit["tanggalAwal"]);
   		 $tanggal2 = mysqli_escape_string($this->conn(), $unit["tanggalAkhir"]);

   		 $rows = NULL;
   		 $query = $this->query("SELECT * FROM  tl_unit WHERE TU_TGL_WAREHOUSE BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY TU_SYS_CREATED DESC");
   		 while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	function getPenjualan($penjualan) {
		$tanggal1 = mysqli_escape_string($this->conn(), $penjualan["tanggalAwal"]);
   		$tanggal2 = mysqli_escape_string($this->conn(), $penjualan["tanggalAkhir"]);

   		$rows = NULL;
   		 $query = $this->query("SELECT A.*, B.* FROM  tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_TANGGAL BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY A.TP_SYS_CREATED DESC");
   		 while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	function ReportStock($tanggal1, $tanggal2) {
   		 $rows = NULL;
   		 $query = $this->query("SELECT * FROM  tl_unit WHERE TU_TGL_WAREHOUSE BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY TU_SYS_CREATED DESC");
   		 while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	function getPenjualanId($tgl1, $tgl2) {
		$rows = NULL;
   		 $query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_TANGGAL BETWEEN '$tgl1' AND '$tgl2' ORDER BY A.TP_SYS_CREATED DESC");
   		 while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	function getReportPiutang($piutang) {
		$tanggal1 = mysqli_escape_string($this->conn(), $piutang["tanggalAwal"]);
   		$tanggal2 = mysqli_escape_string($this->conn(), $piutang["tanggalAkhir"]);

   		$rows = NULL;
   		$query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_TANGGAL BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY A.TP_SYS_CREATED DESC");

   		while($row = $query->fetch_assoc()) {
   			$rows[] = $row;
   		}

   		return $rows;
	}

	function reportPiutang($tanggal1, $tanggal2) {
		$rows = NULL;
   		 $query = $this->query("SELECT A.*, B.* FROM tl_penjualan AS A INNER JOIN tl_leasing AS B ON A.TP_LEASING_ID = B.TL_PRSHID WHERE A.TP_TANGGAL BETWEEN '$tanggal1' AND '$tanggal2' ORDER BY A.TP_SYS_CREATED DESC");
   		 while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;
	}

	//dashboard
	function get_chart_order($bulan) {

    	$tahun = date('Y');

    	$rows = NULL;
		$query = $this->query("SELECT IFNULL(SUM(TC_NOMINAL),0) AS jml FROM tl_cashflow WHERE month(TC_TANGGAL) = '$bulan' AND year(TC_TANGGAL) = '$tahun'");
		while($row = $query->fetch_assoc()) {
    		$rows = $row["jml"];
    	}
    	return $rows;
    }

    function get_chart_penjualan($bulan) {
       $tahun = date('Y');

    	$rows = NULL;
		$query = $this->query("SELECT COUNT(TP_BIGID) AS jml FROM tl_penjualan WHERE month(TP_TANGGAL) = '$bulan' AND year(TP_TANGGAL) = '$tahun'");
		while($row = $query->fetch_assoc()) {
    		$rows = $row["jml"];
    	}
    	return $rows;	
    }

    function get_chart_leasing() {
    	$rows = NULL;
		$query = $this->query("SELECT TL_PRSH_NAMA, TL_JML_PIUTANG FROM tl_leasing WHERE TL_STATUS = 'AKTIF'");
		while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;	
    }

    function get_chart_piutang() {
    	$rows = NULL;
		$query = $this->query("SELECT TL_PRSH_NAMA, TL_NOMINAL_PIUTANG FROM tl_leasing WHERE TL_STATUS = 'AKTIF'");
		while($row = $query->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	return $rows;	
    }
}

?>