<div class="footer text-muted">
		&copy; 2018. <a href="#">LEASING APP</a> -- <a href="dashboard-adm-oh" target="_blank">CV. MAJU</a>
</div>
<script type="text/javascript">
	function archiveFunction() {
	event.preventDefault(); // prevent form submit
	var form = event.target.form; // storing the form
	        swal({
	  title: "Apakah anda ingin menghapus data ini?",
	  text: "Jika dihapus data akan otomatis hilang.",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: "Ya, delete data!",
	  cancelButtonText: "Tidak, cancel!",
	  closeOnConfirm: false,
	  closeOnCancel: false
	},
	function(isConfirm){
	  if (isConfirm) {
	    form.submit();          // submitting the form when user press yes
	  } else {
	    swal("Cancelled", "Data anda masih tersedia :)", "error");
	  }
	});
}
</script>