<?php
use Carbon\Carbon;
class AppController 
{
    public function dateNow()
    {
        return Carbon::now()->format('l j F Y H:i:s');
    }
}
?>