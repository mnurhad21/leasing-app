<?php
$do = explode ("/", $_REQUEST['do']);
$opsi = $do[0];

define('editor', dirname (__FILE__) . '/editor/');
define('admin_dealer', dirname(__FILE__) . '/admin-dealer/');
define('admin_ho', dirname(__FILE__) . '/admin-ho/');
define('admin_leasing', dirname(__FILE__). '/admin-leasing/');
define('profile', dirname(__FILE__) . '/profile/');
define('unit', dirname(__FILE__) . '/admin-ho/unit/');
define('unit_upload', dirname(__FILE__) . '/admin-ho/unit/upload/');
define('unit_first', dirname(__FILE__) . '/admin-ho/unit/unit-first/');
define('cashflow', dirname(__FILE__) . '/admin-ho/cashflow/');
define('nasabah', dirname(__FILE__) . '/admin-dealer/nasabah/');
define('leasing_', dirname(__FILE__). '/admin-ho/leasing/');
define('user_', dirname(__FILE__). '/admin-ho/user/');
define('penjualan', dirname(__FILE__). '/admin-dealer/penjualan/');
define('order', dirname(__FILE__). '/admin-dealer/order/');
define('alokasi', dirname(__FILE__). '/admin-ho/alokasi-piutang/');
define('view', dirname(__FILE__). '/admin-ho/view-order/');
define('adm_leasing_unit', dirname(__FILE__). '/admin-leasing/view-unit/');
define('adm_leasing_order', dirname(__FILE__). '/admin-leasing/view-order/');
//dealer
define('report_unit', dirname(__FILE__). '/admin-dealer/laporan/stock/');
define('report_piutang', dirname(__FILE__). '/admin-dealer/laporan/piutang/');
//admin head
define('report_unit_ho', dirname(__FILE__). '/admin-ho/laporan/stock-unit/');
define('report_piutang_ho', dirname(__FILE__). '/admin-ho/laporan/piutang/');
define('report_foll_ho', dirname(__FILE__). '/admin-ho/laporan/followup/');
define('report_penjualan', dirname(__FILE__). '/admin-ho/laporan/penjualan/');
//dashboard
define('dash', dirname(__FILE__). '/admin-ho/dash-data/');

switch($opsi) {
	case 'login':
		require_once (editor . 'index.php');
	break;
	case 'logout':
		require_once (editor. 'logout.php');
	break;
	case 'dashboard-adm-dealer':
		require_once (admin_dealer. 'index.php');
	break;
	case 'dashboard-adm-oh':
		require_once (admin_ho. 'index.php');
	break;
	case 'dashboard-js':
		require_once(dash. 'index.php');
	break;
	case 'dashboard-adm-leasing':
		require_once(admin_leasing. 'index.php');
	break;
	case 'my-profile':
	   require_once (profile. 'index.php');	
	break;
	case 'setting-account':
		require_once (profile. 'account.php');
	break;
	//stock unit
	case 'stock-unit':
		require_once (unit. 'index.php');
	break;
	case 'edit-unit':
		require_once (unit. 'edit.php');
	break;
	case 'delete-unit':
		require_once (unit. 'delete.php');
	break;
	case 'data-unit':
		require_once (unit. 'data.php');
	break;
	case 'upload-unit':
		require_once (unit_upload. 'index.php');
	break;
	case 'unit-first':
		require_once (unit_first. 'index.php');
	break;
	case 'edit-unit-first':
		require_once(unit_first. 'edit.php');
	break;
	case 'delete-unit-first':
		require_once(unit_first. 'delete.php');
	break;
	//cashflow
	case 'cashflow':
		require_once (cashflow. 'index.php');
	break;
	case 'edit-cashflow':
		require_once (cashflow. 'edit.php');
	break;
	case 'delete-cashflow':
		require_once (cashflow. 'delete.php');
	break;
	//nasabah
	case 'list-nasabah':
		require_once (nasabah. 'index.php');
	break;
	case 'edit-nasabah':
		require_once (nasabah . 'edit.php');
	break;
	case 'delete-nasabah':
		require_once (nasabah . 'delete.php');
	break;
	//leasing
	case 'leasing':
		require_once(leasing_ . 'index.php');
	break;
	case 'edit-leasing':
		require_once(leasing_ . 'edit.php');
	break;
	case 'delete-leasing':
		require_once(leasing_ . 'delete.php');
	break;
	//manajemen user
	case 'management-user':
		require_once(user_. 'index.php');
	break;
	case 'edit-user':
		require_once(user_. 'edit.php');
	break;
	case 'delete-user':
		require_once(user_. 'delete.php');
	break;
	case 'view-penjualan':
		require_once(penjualan . 'index.php');
	break;
	case 'nasabah-id':
		require_once(penjualan . 'data.php');
	break;
	case 'unit-id':
		require_once(penjualan. 'unit.php');
	break;
	case 'alokasi-piutang':
		require_once(alokasi. 'index.php');
	break;
	case 'alokasi-id':
		require_once(alokasi. 'data.php');
	break;
	case 'alokasi-fix':
		require_once(alokasi. 'data_cash.php');
	break;
	case 'alokasi-piutang-leasing':
		require_once(alokasi. 'alokasi.php');
	break;
	case 'status-alokasi':
		require_once(alokasi. 'piutang_status.php');
	break;
	case 'order-status':
		require_once(view. 'set_status.php');
	break;
	case 'order-list':
		require_once(view. 'index.php');
	break;
	case 'data-order':
		require_once(order. 'index.php');
	break;
	case 'confirm-order':
		require_once(order. 'confirm.php');
	break;
	case 'cetak-transaksi':
		require_once(order. 'cetak.php');
	break;
	case 'pdf':
		require_once(order. 'pdf.php');
	break;
	//==== REPORT ====//
	//admin dealer
	case 'report-unit':
		require_once(report_unit. 'index.php');
	break;
	case 'report':
		require_once(report_unit. 'data.php');
	break;
	case 'report-piutang':
		require_once(report_piutang. 'index.php');
	break;
	case 'get-report':
		require_once(report_piutang. 'data.php');
	break;
	//report head
	case 'report-stock-unit':
		require_once(report_unit_ho. 'index.php');
	break;
	case 'laporan-unit':
		require_once(report_unit_ho. 'data.php');
	break;
	case 'report-piutang-followup':
		require_once(report_foll_ho. 'index.php');
	break;
	case 'laporan-followup':
		require_once(report_foll_ho. 'data.php');
	break;
	case 'report-piutang-leasing':
		require_once(report_piutang_ho. 'index.php');
	break;
	case 'report-penjualan':
		require_once(report_penjualan. 'index.php');
	break;
	case 'laporan-penjualan':
		require_once(report_penjualan. 'data.php');
	break;
	//===== END ====//
	case 'list-unit':
		require_once(adm_leasing_unit. 'index.php');
	break;
	case 'list-data-order':
		require_once(adm_leasing_order. 'index.php');
	break;
	case 'history-order':
		require_once(adm_leasing_order. 'history.php');
	break;
	case 'status-order':
		require_once(adm_leasing_order. 'set_status.php');
	break;
	default:
		require_once (editor . 'index.php');
}