<?php 
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

	if(isset($_POST['submitUnit'])){
		$unit['unitId'] = $_POST['unitId'];
	    $unit['unitType'] = $_POST['unitType'];
	    $unit['unitWarna'] = $_POST['unitWarna'];
	    $unit['unitRangka'] = $_POST['unitRangka'];
	    $unit['unitMesin'] = $_POST['unitMesin'];
	    $unit['unitTahun'] = $_POST['unitTahun'];
	    $unit['unitHarga'] = $_POST['unitHarga'];
	    $ctrl = $log->tambahUnit($unit);
	}

	$unit = $log->getUnit();
	$unitData = $log->getDataUnit();

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/adminoh/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master Data</span> - Unit Stock</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Master Data</a></li>
							<li class="active">Unit Stock</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<form action="upload-unit" method="post" enctype="multipart/form-data" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Upload Data Stock Unit</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a href="./layouts/asset/documents/unit.xlsx"><span class="icon-download"></span>  Unduh file contoh</a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">
								<p class="content-group">
						        Unggah file Excel berisi data stock unit, silahkan download file contoh terlebih dahulu.
						      </p>
						      <p class="content-group">
						        Harap pastikan isi dan format file sudah benar.
						      </p>
								<div class="form-group">
									<label class="col-lg-2 control-label">Upload File:</label>
									<div class="col-lg-5">
										<input type="file" name="excel" class="file-styled">
										<span class="help-block">Accepted formats: .xlsx</span>
									</div>
								</div>


								<div class="text-center">
									<button name="submit" class="btn btn-primary">Proses Data<i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<form  method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Tambah Data Stock Unit</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">
								<div class="form-group">
									<label class="col-lg-2 control-label">Unit ID:</label>
									<div class="col-lg-9">
										<input type="text" name="unitId" class="form-control" value="SP<?php echo rand(1111,9999); ?>" placeholder="Unit ID">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Type:</label>
									<div class="col-lg-9">
										<!-- <input type="text" name="unitType" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Type"> -->
										<select class="select" name="unitTypeId" id="id" onchange="cek_database()" required>
											<optgroup label="Customer ID">
												<option value="">Pilih Type</option>
												<?php foreach($unitData as $nsbh){ ?>
												<option value="<?php echo $nsbh['TUD_ID']; ?>"><?php echo $nsbh['TUD_NAMA']; ?></option>
												<?php  } ?>
											</optgroup>
										</select>
										<input type="hidden" name="unitType" id="unitType">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Harga/Unit:</label>
									<div class="col-lg-9">
										<div class="input-group">
											<label class="input-group-addon">Rp.</label>
											<input type="text" name="unitHarga" id="unitHarga" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Harga Satuan">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Warna:</label>
									<div class="col-lg-9">
										<input type="text" name="unitWarna" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Warna">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nomer Rangka:</label>
									<div class="col-lg-9">
										<input type="text" name="unitRangka" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nomer Rangka">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nomer Mesin:</label>
									<div class="col-lg-9">
										<input type="text" name="unitMesin" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="No Mesin">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Tahun Keluar:</label>
									<div class="col-lg-9">
										<input type="text" name="unitTahun" class="form-control" onkeyup="this.value = this.value.toUpperCase()" value="<?php  echo date('Y'); ?>" placeholder="Tahun Keluar Kendaraan">
									</div>
								</div>

								<div class="text-right">
									<button name="submitUnit" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Scrollable datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Unit Stock</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>
						<table class="table datatable-scroll-y" width="100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Unit ID</th>
									<th>Type</th>
									<th>Warna</th>
									<th>Nomer Rangka</th>
									<th>Nomer Mesin</th>
									<th>Tahun</th>
									<th>Harga Unit</th>
									<th>Status</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 1; ?>
								<?php if(is_array($unit) || is_object($unit)) { ?>
								<?php foreach($unit as $row) { ?>
								<tr>
									<td><?= $no++; ?></td>
									<td><?= $row["TU_UNITID"]; ?></td>
									<td><?= $row["TU_TYPE"]; ?></td>
									<td><?= $row["TU_WARNA"]; ?></td>
									<td><?= $row["TU_NOMER_RANGKA"]; ?></td>
									<td><?= $row["TU_NOMER_MESIN"]; ?></td>
									<td><?= $row["TU_TAHUN"]; ?></td>
									<td><?= number_format($row["TU_HARGA"],2,',','.'); ?></td>
									<?php if($row["TU_STATUS"] == "READY") { ?>
									<td><span class="label label-success">READY</span></td>
									<?php } else { ?>
									<td><span class="label label-danger">KOSONG</span></td>
									<?php } ?>
									<td class="text-center">
										<a href="edit-unit?data-unit=<?= $row["TU_UNITID"]; ?>" class="btn btn-warning"><span class="icon-pencil6"></span></a>
										<a href="delete-unit?data-unit=<?= $row["TU_UNITID"]; ?>" class="btn btn-danger delete-link"><span class="icon-trash"></span></a>
									</td>
								</tr>
								<?php }} ?>
							</tbody>
						</table>
					</div>
					<!-- /scrollable datatable -->
					 


					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	 <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: 'Apakah Anda Yakin hapus data?',
                        text: 'Data yang telah dihapus akan hilang',
                        type: 'warning',
                        html: true,
                        confirmButtonColor: '#d9534f',
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
    </script>
     <script type="text/javascript">
    	function cek_database(){
        var custId = $("#id").val();
       // console.log(custId);
        $.ajax({
            url: 'data-unit',
            data:"id="+custId,
        }).success(function (data) {
            var json = data,
            obj = JSON.parse(json);
            $('#unitHarga').val(obj.unitHarga);
            $('#unitType').val(obj.unitType);
        });
    }
    </script>
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

