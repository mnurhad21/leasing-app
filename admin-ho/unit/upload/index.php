<?php
session_start();
require 'vendor/autoload.php';
require_once 'query/config.php';

$conn = new Connection();

$user_id = $_SESSION["U_ID"];
$random = rand(111,999);
$datetime = date('Y-m-d H:i:s');
$date = date("Y-m-d");
$status = 'READY';
//target excel
$targetName = $_FILES['excel']['name'];
$extensi = explode('.', $targetName);
$filename = "file-".round(microtime(true)).".".end($extensi);
$target = $_FILES['excel']['tmp_name'];
$target_dir = 'library/document/';
$target_file = $target_dir.$filename;
$upload = move_uploaded_file($target, $target_file);

$obj = PHPExcel_IOFactory::load($target_file);
$all_data = $obj->getActiveSheet()->toArray(null, true, true ,true);

//insert ke tabel upload

$sql = "INSERT INTO tl_unit (TU_UP_ID, TU_UNITID, TU_TYPE, TU_WARNA, TU_NOMER_RANGKA, TU_NOMER_MESIN, TU_HARGA, TU_TAHUN, TU_CREATED_USER, TU_STATUS, TU_TGL_WAREHOUSE, TU_SYS_CREATED) VALUES";
for($i=1; $i <= count($all_data); $i++) {
	$unitId = $all_data[$i]['A'];
	$type = $all_data[$i]['B'];
	$warna = $all_data[$i]['C'];
	$no_rangka = $all_data[$i]['D'];
	$no_mesin = $all_data[$i]['E'];
	$harga = $all_data[$i]['F'];
	$tahun = $all_data[$i]['G'];
	$sql .= "('$random', '$unitId','$type', '$warna', '$no_rangka', '$no_mesin', '$harga', '$tahun', '$user_id', '$status', '$date', '$datetime'),";
}

$sql = substr($sql, 0, -1);
//echo $sql;
$query = $conn->query($sql);

if($query) {
	$query1 = $conn->query("INSERT INTO tl_upload (TP_VALUE, TP_TGL, TP_VALUE_PATH, TP_USER_ID) VALUES ('$random', '$date', '$target_file', '$user_id')");
	echo "<script>alert('Stock Unit Berhasil diupload')
			location.replace('stock-unit')</script></script>";
} else {
	echo "<script>alert('Stock Unit Berhasil diupload')
			location.replace('stock-unit')</script></script>";
}
unlink($target_file);

?>