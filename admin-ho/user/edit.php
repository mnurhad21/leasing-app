<?php
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
 session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

	if(isset($_POST['submit'])){
		$userData['username'] = $_POST['username'];
	    $userData['nama'] = $_POST['nama'];
	    $userData['email'] = $_POST['email'];
	    $userData['noTelp'] = $_POST['noTelp'];
	    $userData['rule'] = $_POST['rule'];
	    $userData['status'] = $_POST['status'];
	    $ctrl = $log->updateUser($userData);
	}

	$user = $log->getUserById($_GET['data-mngmnt']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/picker_date.js"></script>

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/adminoh/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Edit Data</span> - Manajemen User</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Edit Data</a></li>
							<li class="active">Managemen User</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<form action="" method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Edit Data User - <?= $user['U_FULLNAME']; ?></h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">

								<div class="form-group">
									<label class="col-lg-2 control-label">Username ID:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" value="<?php echo $user['U_ID']; ?>" name="username" onkeyup="this.value = this.value.toUpperCase()" placeholder="Username" disabled>
										<input type="hidden" name="username" value="<?php echo $user['U_ID']; ?>">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nama Lengkap:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" value="<?php echo $user['U_FULLNAME']; ?>" name="nama" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Lengkap" required>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Email:</label>
									<div class="col-lg-9">
										<input type="email" class="form-control" value="<?php echo $user['U_EMAIL']; ?>" name="email" autocomplete="off" placeholder="email" required>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-lg-2 control-label">No Telp:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" value="<?php echo $user['U_TELPON']; ?>" name="noTelp" onkeypress="return hanyaAngka(event)" placeholder="No Telp">
									</div>
								</div>								

								<div class="form-group">
									<label class="col-lg-2 control-label">RULE:</label>
									<div class="col-lg-9">
										<select class="select" name="rule">		
												<option value="GR_ADMIN_HEAD"<?php if($user['U_GROUP_RULE'] == 'GR_ADMIN_HEAD') {echo "selected";} ?>>ADMIN HEAD</option>
												<option value="GR_ADMIN_DEALER" <?php if($user['U_GROUP_RULE'] == 'GR_ADMIN_DEALER') {echo "selected";} ?>>ADMIN DEALER</option>
												<option value="GR_ADMIN_LEASING"<?php if($user['U_GROUP_RULE'] == 'GR_ADMIN_LEASING') {echo "selected";} ?>>ADMIN LEASING</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Status:</label>
									<div class="col-lg-9">
										<select class="select" name="status">
											<optgroup label="Status">
												<option value="USER_ACTIVE"<?php if($user["U_STATUS"] == "USER_ACTIVE") {echo "selected";} ?>>AKTIF</option>
												<option value="USER_INAKTIF" <?php if($user["U_STATUS"] == "USER_INAKTIF") {echo "selected";} ?>>INAKTIF</option>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="text-right">
									<a href="mngmt-user" class="btn btn-danger">Cancel <i class="icon-undo2"></i></a>
									<button name="submit" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	 <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: 'Apakah Anda Yakin hapus data?',
                        text: 'Data yang telah dihapus akan hilang',
                        type: 'warning',
                        html: true,
                        confirmButtonColor: '#d9534f',
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
    </script>
    <script type="text/javascript">
    	 function hanyaAngka(evt) {
		    var charCode = (evt.which) ? evt.which : event.keyCode
		    if (charCode > 31 && (charCode < 48 || charCode > 57))
		      return false;
		    return true;
		    if(charCode.length != 10) {
		      alert("Nomer Telpon harus 11 digit");
		      return false;
		    }
		  }
    </script>
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

