<?php 
session_start(); 
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {

include_once ('./query/model.php');
$log = new Model();

if(isset($_POST['image_back']) && $_FILES['IMG_PATH']){
	$back['userId'] = $_POST['userId'];
    $back['IMG_PATH'] = $_FILES['IMG_PATH']['name'];
    $ctrl = $log->updateBack($back);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/visualization/echarts/echarts.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/user_pages_profile.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color: #bb1b05">
				<?php 
				if($_SESSION["U_GROUP_RULE"] == "GR_ADMIN_DEALER") {
					include_once './layouts/admindealer/sidebar.php';
				} elseif($_SESSION["U_GROUP_RULE"] == "GR_ADMIN_HEAD") {
				 	include_once './layouts/adminoh/sidebar.php';
				} else {
					include_once './layouts/adminleasing/sidebar.php';
				}
				?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header no-padding">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">User Pages</span> - <?php echo $_SESSION["U_FULLNAME"]; ?></h4>
						</div>
					</div>

					<div class="breadcrumb-line breadcrumb-line-wide no-margin">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="user_pages_profile_cover.html">User pages</a></li>
							<li class="active">Profile cover</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Cover area -->
				<div class="profile-cover">
					<div class="profile-cover-img" style="background-image: url(./layouts/asset/assets/images/back/<?php echo $_SESSION["U_BACKGROUND"]; ?>)"></div>
					<div class="media">
						<div class="media-left">
							<a href="#" class="profile-thumb">
								<img src="./layouts/asset/assets/images/profile/<?php echo $_SESSION["IMG_AVATAR"]; ?>" class="img-circle" alt="">
							</a>
						</div>

						<div class="media-body">
				    		<h1><?php echo $_SESSION["U_FULLNAME"]; ?><small class="display-block"><?php if($_SESSION["U_GROUP_RULE"] == "GR_ADMIN_LEASING") {echo "Admin OH";} else {echo "Admin Dealer";} ?></small></h1>
						</div>

						<div class="media-right media-middle">
							<ul class="list-inline list-inline-condensed no-margin-bottom text-nowrap">
								<form action="" method="post" class="form-horizontal" id="myForm" enctype="multipart/form-data">
									<input type="hidden" name="userId" value="<?php echo $_SESSION["U_ID"]; ?>">
									<li><input type="file" name="IMG_PATH" class="file-styled" value="Cover"></li>
									<li><button class="btn btn-primary" name="image_back"><i class="icon-file-picture position-left"></i> Cover image</button></li>
								</form>
							</ul>
						</div>
					</div>
				</div>
				<!-- /cover area -->


				<!-- Toolbar -->
				<div class="navbar navbar-default navbar-xs content-group">
					<ul class="nav navbar-nav visible-xs-block">
						<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
					</ul>

					<div class="navbar-collapse collapse" id="navbar-filter">
						<ul class="nav navbar-nav element-active-slate-400">
							<li class="active"><a href="#activity" data-toggle="tab"><i class="icon-menu7 position-left"></i> Activity</a></li>
						</ul>

						<div class="navbar-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="icon-stack-text position-left"></i> Profile <?php echo $_SESSION["U_FULLNAME"]; ?></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- /toolbar -->


				<!-- Content area -->
				<div class="content">

					<!-- User profile -->
					<div class="row">
						<div class="col-lg-12">
							<div class="tabbable">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="activity">

										<!-- Timeline -->
										<div class="timeline timeline-left content-group">
											<div class="timeline-container">

												<!-- Sales stats -->
												<div class="timeline-row">
													<div class="timeline-icon">
														<a href="#"><img src="assets/images/placeholder.jpg" alt=""></a>
													</div>

													<div class="panel panel-flat timeline-content">
														<div class="panel-heading">
															<h6 class="panel-title">Daily Profile</h6>
															<div class="heading-elements">
																<span class="heading-text"><i class="icon-history position-left text-success"></i> Updated <?php echo date("d F Y H:i:s"); ?></span>

																<ul class="icons-list">
											                		<li><a data-action="reload"></a></li>
											                	</ul>
										                	</div>
														</div>

														<div class="panel-body">
															<li class="media">
																<a href="#" class="media-link">
																	<div class="media-left"></div>
																	<div class="media-body">
																		<span class="media-heading text-semibold"><i class="icon-user"></i>      <?php echo $_SESSION["U_ID"]; ?></span>
																		<span class="media-annotation"><i class="icon-user-check"></i>   <?php echo $_SESSION["U_FULLNAME"]; ?></span><br>
																		<span class="media-annotation"><i class="icon-mail5"></i>      <?php echo $_SESSION["U_EMAIL"]; ?></span><br>
																		<span class="media-annotation"><i class="icon-phone2"></i>  	<?php echo $_SESSION["U_TELPON"]; ?></span>
																	</div>
																	<div class="media-right media-middle">
																		<span class="status-mark bg-danger"></span>
																	</div>
																</a>
															</li>
														</div>
													</div>
												</div>
												<!-- /sales stats -->

											</div>
									    </div>

									    <!-- /timeline -->

									</div>

									<div class="tab-pane fade" id="schedule">

										<!-- Available hours -->
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h6 class="panel-title">Available hours</h6>
												<div class="heading-elements">
													<ul class="icons-list">
								                		<li><a data-action="collapse"></a></li>
								                		<li><a data-action="reload"></a></li>
								                		<li><a data-action="close"></a></li>
								                	</ul>
							                	</div>
											</div>

											<div class="panel-body">
												<div class="chart-container">
													<div class="chart has-fixed-height" id="plans"></div>
												</div>
											</div>
										</div>
										<!-- /available hours -->


										<!-- Calendar -->
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h6 class="panel-title">My schedule</h6>
												<div class="heading-elements">
													<ul class="icons-list">
								                		<li><a data-action="collapse"></a></li>
								                		<li><a data-action="reload"></a></li>
								                		<li><a data-action="close"></a></li>
								                	</ul>
							                	</div>
											</div>

											<div class="panel-body">
												<div class="schedule"></div>
											</div>
										</div>
										<!-- /calendar -->

									</div>

									<div class="tab-pane fade" id="settings">

										<!-- Profile info -->
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h6 class="panel-title">Profile information</h6>
												<div class="heading-elements">
													<ul class="icons-list">
								                		<li><a data-action="collapse"></a></li>
								                		<li><a data-action="reload"></a></li>
								                		<li><a data-action="close"></a></li>
								                	</ul>
							                	</div>
											</div>

											<div class="panel-body">
												<form action="#">
													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<label>Username</label>
																<input type="text" value="Eugene" class="form-control">
															</div>
															<div class="col-md-6">
																<label>Full name</label>
																<input type="text" value="Kopyov" class="form-control">
															</div>
														</div>
													</div>

													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<label>Address line 1</label>
																<input type="text" value="Ring street 12" class="form-control">
															</div>
															<div class="col-md-6">
																<label>Address line 2</label>
																<input type="text" value="building D, flat #67" class="form-control">
															</div>
														</div>
													</div>

													<div class="form-group">
														<div class="row">
															<div class="col-md-4">
																<label>City</label>
																<input type="text" value="Munich" class="form-control">
															</div>
															<div class="col-md-4">
																<label>State/Province</label>
																<input type="text" value="Bayern" class="form-control">
															</div>
															<div class="col-md-4">
																<label>ZIP code</label>
																<input type="text" value="1031" class="form-control">
															</div>
														</div>
													</div>

													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<label>Email</label>
																<input type="text" readonly="readonly" value="eugene@kopyov.com" class="form-control">
															</div>
															<div class="col-md-6">
									                            <label>Your country</label>
									                            <select class="select">
									                                <option value="germany" selected="selected">Germany</option> 
									                                <option value="france">France</option> 
									                                <option value="spain">Spain</option> 
									                                <option value="netherlands">Netherlands</option> 
									                                <option value="other">...</option> 
									                                <option value="uk">United Kingdom</option> 
									                            </select>
															</div>
														</div>
													</div>

							                        <div class="form-group">
							                        	<div class="row">
							                        		<div class="col-md-6">
																<label>Phone #</label>
																<input type="text" value="+99-99-9999-9999" class="form-control">
																<span class="help-block">+99-99-9999-9999</span>
							                        		</div>

															<div class="col-md-6">
																<label>Upload profile image</label>
							                                    <input type="file" class="file-styled">
							                                    <span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>
															</div>
							                        	</div>
							                        </div>

							                        <div class="text-right">
							                        	<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
							                        </div>
												</form>
											</div>
										</div>
										<!-- /profile info -->


										<!-- Account settings -->
										<div class="panel panel-flat">
											<div class="panel-heading">
												<h6 class="panel-title">Account settings</h6>
												<div class="heading-elements">
													<ul class="icons-list">
								                		<li><a data-action="collapse"></a></li>
								                		<li><a data-action="reload"></a></li>
								                		<li><a data-action="close"></a></li>
								                	</ul>
							                	</div>
											</div>

											<div class="panel-body">
												<form action="#">
													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<label>Username</label>
																<input type="text" value="Kopyov" readonly="readonly" class="form-control">
															</div>

															<div class="col-md-6">
																<label>Current password</label>
																<input type="password" value="password" readonly="readonly" class="form-control">
															</div>
														</div>
													</div>

													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<label>New password</label>
																<input type="password" placeholder="Enter new password" class="form-control">
															</div>

															<div class="col-md-6">
																<label>Repeat password</label>
																<input type="password" placeholder="Repeat new password" class="form-control">
															</div>
														</div>
													</div>

													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<label>Profile visibility</label>

																<div class="radio">
																	<label>
																		<input type="radio" name="visibility" class="styled" checked="checked">
																		Visible to everyone
																	</label>
																</div>

																<div class="radio">
																	<label>
																		<input type="radio" name="visibility" class="styled">
																		Visible to friends only
																	</label>
																</div>

																<div class="radio">
																	<label>
																		<input type="radio" name="visibility" class="styled">
																		Visible to my connections only
																	</label>
																</div>

																<div class="radio">
																	<label>
																		<input type="radio" name="visibility" class="styled">
																		Visible to my colleagues only
																	</label>
																</div>
															</div>

															<div class="col-md-6">
																<label>Notifications</label>

																<div class="checkbox">
																	<label>
																		<input type="checkbox" class="styled" checked="checked">
																		Password expiration notification
																	</label>
																</div>

																<div class="checkbox">
																	<label>
																		<input type="checkbox" class="styled" checked="checked">
																		New message notification
																	</label>
																</div>

																<div class="checkbox">
																	<label>
																		<input type="checkbox" class="styled" checked="checked">
																		New task notification
																	</label>
																</div>

																<div class="checkbox">
																	<label>
																		<input type="checkbox" class="styled">
																		New contact request notification
																	</label>
																</div>
															</div>
														</div>
													</div>

							                        <div class="text-right">
							                        	<button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
							                        </div>
						                        </form>
											</div>
										</div>
										<!-- /account settings -->

									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /user profile -->


					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>
