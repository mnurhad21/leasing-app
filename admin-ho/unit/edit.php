<?php 
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

//get data get_list_armada
	$driver = $log->get_unit_id($_GET['data-unit']);

	if(isset($_POST['ubah'])){
	 $unit['unitId']             = $_POST['unitId'];
	 $unit['unitType']               = $_POST['unitType'];
	 $unit['unitWarna']              = $_POST['unitWarna'];
	 $unit['unitRangka']             = $_POST['unitRangka'];
	 $unit['unitMesin']             = $_POST['unitMesin'];
	 $unit['unitStatus']         = $_POST['unitStatus'];
	 $unit['unitTahun']         = $_POST['unitTahun'];
	 $unit['unitHarga']         = $_POST['unitHarga'];
	 $ctrl = $log->editUnit($unit);
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/adminoh/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Master Data</span> - Unit Stock</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Edit Data</a></li>
							<li class="active">Unit Stock</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<form  method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Tambah Data Stock Unit</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">
								<div class="form-group">
									<label class="col-lg-2 control-label">Unit ID:</label>
									<div class="col-lg-9">
										<input type="text" name="unitId" value="<?= $driver["TU_UNITID"]; ?>" disabled class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Unit ID">
										<input type="hidden" name="unitId" value="<?= $driver["TU_UNITID"]; ?>">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Type:</label>
									<div class="col-lg-9">
										<input type="text" name="unitType" class="form-control" value="<?= $driver["TU_TYPE"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="Type">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Warna:</label>
									<div class="col-lg-9">
										<input type="text" name="unitWarna" class="form-control" value="<?= $driver["TU_WARNA"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="Warna">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nomer Rangka:</label>
									<div class="col-lg-9">
										<input type="text" name="unitRangka" class="form-control" value="<?= $driver["TU_NOMER_RANGKA"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nomer Rangka">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nomer Mesin:</label>
									<div class="col-lg-9">
										<input type="text" name="unitMesin" class="form-control" value="<?= $driver["TU_NOMER_MESIN"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="No Mesin">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Status Unit:</label>
									<div class="col-lg-9">
										<select class="select" name="unitStatus">
											<optgroup label="Status Unit">
												<option value="READY"<?php if($driver["TU_STATUS"] == "READY") {echo "selected";} ?>>READY</option>
												<option value="KOSONG" <?php if($driver["TU_STATUS"] == "KOSONG") {echo "selected";} ?>>KOSONG</option>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Tahun Keluar:</label>
									<div class="col-lg-9">
										<input type="text" name="unitTahun" class="form-control" value="<?= $driver["TU_TAHUN"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="Tahun Keluar Kendaraan">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Harga/Unit:</label>
									<div class="col-lg-9">
										<div class="input-group">
											<label class="input-group-addon">Rp.</label>
											<input type="text" name="unitHarga" value="<?= $driver["TU_HARGA"]; ?>" class="form-control" onkeyup="this.value = this.value.toUpperCase()" placeholder="Harga Satuan">
										</div>
									</div>
								</div>


								<div class="text-right">
									<a href="stock-unit" class="btn btn-danger">Cancel <i class="icon-undo2"></i></a>
									<button name="ubah" class="btn btn-primary">Submit form <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

