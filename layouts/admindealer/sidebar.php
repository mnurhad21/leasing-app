<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="asset/assets/images/profile/<?php echo $_SESSION["IMG_AVATAR"]; ?>" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?php echo $_SESSION["U_FULLNAME"]; ?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Admin Dealer
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>SETTING INFORMATION</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="active"><a href="dashboard-adm-dealer"><i class="icon-home2"></i> <span>Dashboard</span></a></li>
								<li>
									<a href="#"><i class="icon-stack2"></i> <span>Data Master</span></a>
									<ul>
										<li><a href="list-nasabah"><span class="icon-users"></span> Data Nasabah</a></li>
										<!-- <li><a href="data-leasing"><span class="icon-office"></span> Data Leasing</a></li> -->
										<!-- <li><a href="mngmt-user"><span class="icon-user-tie"></span> Manajemen User</a></li> -->
									</ul>
								</li>
								<li><a href="view-penjualan"><i class="icon-list-unordered"></i> <span>Transaksi Penjualan</span></a></li>
								<!-- <li><a href="changelog.html"><i class="icon-list-unordered"></i> <span>Data Order</span></a></li> -->
								<li>
									<a href="#"><i class="icon-droplet2"></i> <span>Data Order</span></a>
									<ul>
										<li><a href="data-order"><span class="icon-notebook"></span> List Order</a></li>
										<!-- <li><a href="confirm-order"><span class="icon-stack-check"></span> Konfirmasi Order</a></li> -->
									</ul>
								</li>
								<li>
									<a href="#"><i class="icon-stack"></i> <span>Laporan</span></a>
									<ul>
										<li><a href="report-unit"><span class="icon-database-refresh"></span> Stock Unit</a></li>
										<li><a href="report-piutang"><span class=" icon-credit-card"></span> Penjualan</a></li>
										<!-- <li><a href="report-followup"><span class="icon-user-check"></span> Follow Up Leasing</a></li> -->
									</ul>
								</li>
								<!-- <li><a href="changelog.html"><i class="icon-list-unordered"></i> <span>Changelog</span></a></li> -->
								<!-- /main -->
								<!-- Data visualization -->
								<li class="navigation-header"><span>USER INFORMARTION</span> <i class="icon-menu" title="Data visualization"></i></li>
								<li><a href="my-profile"><i class=" icon-user-tie"></i> <span>Profil</span></a></li>
								<li><a href="setting-account"><i class=" icon-gear"></i> <span>Setting</span></a></li>
								<!-- /tables -->
								<!-- /page kits -->

							</ul>
						</div>
					</div>
					<!-- /main navigation -->
				</div>