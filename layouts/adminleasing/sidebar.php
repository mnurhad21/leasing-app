<div class="sidebar-content">
	<!-- User menu -->
	<div class="sidebar-user">
		<div class="category-content">
			<div class="media">
				<a href="#" class="media-left"><img src="./layouts/asset/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
				<div class="media-body">
					<span class="media-heading text-semibold"><?php echo $_SESSION["U_FULLNAME"]; ?></span>
					<div class="text-size-mini text-muted">
						<i class="icon-pin text-size-small"></i> &nbsp;<?php echo $_SESSION["PRSH_NAMA"]; ?>
					</div>
				</div>

				<div class="media-right media-middle">
					<ul class="icons-list">
						<li>
							<a href="#"><i class="icon-cog3"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- /user menu -->

	<!-- Main navigation -->
	<div class="sidebar-category sidebar-category-visible">
		<div class="category-content no-padding">
			<ul class="navigation navigation-main navigation-accordion">

				<!-- Main -->
				<li class="navigation-header"><span style="color: #fff">DASHBOARD ADMINISTRATOR LEASING</span> <i class="icon-menu" title="Main pages"></i></li>
				<li class="active"><a href="dashboard-adm-leasing"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
				<!-- <li><a href="list-unit"><i class="icon-list-unordered"></i> <span>Stock Unit</span></a></li> -->
				<li>
					<a href="#"><i class="icon-stack2"></i> <span>Transaksi Order</span></a>
					<ul>
						<li><a href="list-data-order"><i class="icon-cash4"></i> View Order</a></li>
						<li><a href="history-order"><i class="icon-history"></i> History</a></li>
					</ul>
				</li>
				<!-- /main -->

				<!-- Forms -->
				<li class="navigation-header"><span>SETTING INFORMATION</span> <i class="icon-user-tie" title="Forms"></i></li>
				<!-- <li><a href="changelog.html"><i class="icon-list-unordered"></i> <span>Manajemen User</span></a></li> -->
				<li><a href="my-profile"><i class="icon-user-tie"></i> <span>Profil</span></a></li>
				<li><a href="setting-account"><i class="icon-gear"></i> <span>Setting</span></a></li>
				<!-- /forms -->
				<!-- /appearance -->

				<!-- /page kits -->

			</ul>
		</div>
	</div>
	<!-- /main navigation -->
</div>