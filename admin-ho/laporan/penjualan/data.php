<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasilreportpenjualan.xls");
include_once ('./query/model.php');
	$log = new Model();
    $ctrl = $log->getPenjualanId($_GET["tanggalAwal"], $_GET["tanggalAkhir"]);
?>
<style type="text/css">
	.table1 {
	    font-family: sans-serif;
	    color: #444;
	    border-collapse: collapse;
	    width: 50%;
	    border: 1px solid #f2f5f7;
	}
	 
	.table1 tr th{
	    background: #000;
	    color: #fff;
	    font-weight: 5px;
	}
	 
	.table1, th, td {
	    padding: 5px 20px;
	}

	.table1, td {
		font-size: 14px;
	}
	 
	.table1 tr:hover {
	    background-color: #f5f5f5;
	}

	.table1 tr:nth-child(even) {
	    background-color: #f2f2f2;
	} 

</style>
<table id="example" class="table1" style="width:100%">
	<thead>
		<tr>
			<th>No</th>
			<th>Tanggal</th>
			<th class="text-center">No ID</th>
			<th>Customer ID</th>
			<th>Nama</th>
			<th>No Telp</th>
			<th class="text-center">Alamat</th>
			<th>Type Penjualan</th>
			<th class="text-center">Leasing ID</th>
			<th class="text-center">Nama Leasing</th>
			<th>Unit ID</th>
			<th>Type</th>
			<th>Warna</th>
			<th>No Mesin</th>
			<th>No Rangka</th>
			<th>Harga Unit</th>
			<th>DP</th>
			<th>Piutang</th>
			<th>OD</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<?php
		 $no = 1;
		 $dateNow = date("Y-m-d");
		 if(is_array($ctrl) || is_object($ctrl)) {
		 foreach($ctrl as $row) { ?>
		<tr>
			<td><?= $no++; ?></td>
			<td><?= $log->TanggalIndo($row["TP_TANGGAL"]); ?></td>
			<td><?= $row["TP_NOID"]; ?></td>
			<td><?= $row["TP_CUST_ID"]; ?></td>
			<td><?= $row["TP_NAMACUST"]; ?></td>
			<td><?= $row["TP_NOHP"]; ?></td>
			<td><?= $row["TP_ALAMAT"]; ?></td>
			<td><?= $row["TP_TYPE_PENJUALAN"]; ?></td>
			<td><?= $row["TP_LEASING_ID"]; ?></td>
			<td class="text-center"><?= $row["TL_PRSH_NAMA"]; ?></td>
			<td><?= $row["TP_UNIT_ID"]; ?></td>
			<td><?= $row["TP_TYPE_NAMA"]; ?></td>
			<td><?= $row["TP_WARNA"]; ?></td>
			<td><?= $row["TP_NO_MESIN"]; ?></td>
			<td><?= $row["TP_NO_RANGKA"]; ?></td>
			<td>Rp. <?= number_format($row["TP_HARGA"],2,',','.'); ?></td>
			<td>Rp. <?= number_format($row["TP_DP"],2,',','.'); ?></td>
			<td>Rp. <?= number_format($row["TP_HASIL"],2,',','.'); ?></td>
			<td><?php $booking    =new DateTime($row['TP_TANGGAL']);
                $today        =new DateTime();
                $diff = $today->diff($booking);
                echo $diff->d; echo " Hari";
            ?></td>
            <td><?= $row["TP_KETERANGAN"]; ?></td>
		</tr>
		<?php } }?>
	</tbody>
</table>