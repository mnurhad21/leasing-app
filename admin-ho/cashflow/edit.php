<?php 
header('Expires: Mon, 1 Jul 1998 01:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
session_start();
if(!empty($_SESSION["U_ID"]) && !empty($_SESSION["U_LOGIN_TOKEN"])) {
	include_once ('./query/model.php');
	$log = new Model();

	if(isset($_POST['submitCashflow'])){
		$cashflowEdit['noId'] = $_POST['noId'];
	    $cashflowEdit['tanggal'] = $_POST['tanggal'];
	    $cashflowEdit['namaBank'] = $_POST['namaBank'];
	    $cashflowEdit['namaAkun'] = $_POST['namaAkun'];
	    $cashflowEdit['terimaDari'] = $_POST['terimaDari'];
	   	$cashflowEdit['totalNominal'] = $_POST['totalNominal'];
	   	$cashflowEdit['keterangan'] = $_POST['keterangan'];
	   	$cashflowEdit['cashflowStatus'] = $_POST['cashflowStatus'];
	    $ctrl = $log->editCashflow($cashflowEdit);
	}

	$driver = $log->get_cashflow_id($_GET['data-cashflow']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LEASING APP</title>
	<link rel="shortcut icon" href="./layouts/asset/assets/images/logoapp.jpego">

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="./layouts/asset/assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/app.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/datatables_basic.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/pages/form_layouts.js"></script>
	<!-- /theme JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/sweet_alert.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/components_modals.js"></script>

	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/datepicker.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/core/libraries/jquery_ui/effects.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/notifications/jgrowl.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/plugins/pickers/pickadate/legacy.js"></script>
	<script type="text/javascript" src="./layouts/asset/assets/js/pages/picker_date.js"></script>

</head>

<body>

	<!-- Main navbar -->
	<?php include_once './layouts/navbar.php'; ?>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main" style="background-color:  #bb1b05">
				<?php include_once './layouts/adminoh/sidebar.php'; ?>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Data</span> - Penerimaan Uang Masuk</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_basic.html">Cashflow</a></li>
							<li class="active">Cashflow</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<form action="" method="post" class="form-horizontal">
						<div class="panel panel-flat">
							<div class="panel-heading">
								<h5 class="panel-title">Tambah Data Peneriamaan Uang Masuk</h5>
								<div class="heading-elements">
									<ul class="icons-list">
				                		<li><a data-action="collapse"></a></li>
				                		<li><a data-action="reload"></a></li>
				                		<li><a data-action="close"></a></li>
				                	</ul>
			                	</div>
							</div>

							<div class="panel-body">

								<div class="form-group">
									<label class="col-lg-2 control-label">Tanggal:</label>
									<div class="col-lg-9">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar"></i></span>
											<input type="text" class="form-control datepicker" value="<?php echo $driver["TC_TANGGAL"]; ?>" name="tanggal" placeholder="Pilih Tanggal&hellip;">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">No ID:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="noId" disabled value="<?php echo $driver["TC_TRANS_ID"]; ?> " placeholder="No ID">
										<input type="hidden" name="noId" value="<?php echo $driver["TC_TRANS_ID"]; ?>">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nama Bank:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="namaBank" value="<?php echo $driver["TC_NAMA_BANK"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Bank">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Nama Akun:</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="namaAkun" value="<?php echo $driver["TC_NAMA_AKUN"]; ?>" onkeyup="this.value = this.value.toUpperCase()" placeholder="Nama Akun">
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Terima dari:</label>
									<div class="col-lg-9">
										<select class="select" name="terimaDari">
											<optgroup label="Nama Leasing">
												<option>Pilih Leasing</option>
												<?php if(is_array($leasing)) {
												 foreach($leasing as $row) {
												 ?>
												<option value="<?php echo $row['TL_PRSHID']; ?>"<?php if($row['TL_PRSHID']) {echo "selected";} ?>><?php echo $row['TL_PRSH_NAMA']; ?></option>
												<?php } } ?>
											</optgroup>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Total Nominal:</label>
									<div class="col-lg-9">
										<div class="input-group">
											<span class="input-group-addon">Rp.</span>
											<input type="text" name="totalNominal" value="<?php echo $driver["TC_NOMINAL"]; ?>" class="form-control" id="inputku" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" placeholder="Total Nominal">
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Keterangan:</label>
									<div class="col-lg-9">
										<textarea rows="5" cols="5" name="keterangan" class="form-control" placeholder="Enter your message here"><?php echo $driver["TC_KETERANGAN"]; ?></textarea>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label">Status:</label>
									<div class="col-lg-9">
										<select class="select" name="cashflowStatus">
											<optgroup label="Status">
												<option value="CONFIRM"<?php if($driver["TC_STATUS"] == "CONFIRM") {echo "selected";} ?>>CONFIRM</option>
												<option value="ON_CHECKING" <?php if($driver["TC_STATUS"] == "ON_CHECKING") {echo "selected";} ?>>WAITING</option>
											</optgroup>
										</select>
									</div>
								</div>


								<div class="text-right">
									<a href="cashflow" class="btn btn-danger">Cancel <i class="icon-undo2"></i></a>
									<button name="submitCashflow" class="btn btn-primary">Update<i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</div>
						</div>
					</form>

					<!-- Footer -->
					<?php include_once './layouts/footer.php'; ?>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	 <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.delete-link').on('click',function(){
                var getLink = $(this).attr('href');
                swal({
                        title: 'Apakah Anda Yakin hapus data?',
                        text: 'Data yang telah dihapus akan hilang',
                        type: 'warning',
                        html: true,
                        confirmButtonColor: '#d9534f',
                        showCancelButton: true,
                        },function(){
                        window.location.href = getLink
                    });
                return false;
            });
        });
    </script>
    <script type="text/javascript">
    	function tandaPemisahTitik(b){
			var _minus = false;
			if (b<0) _minus = true;
				b = b.toString();
				b=b.replace(".","");
				b=b.replace("-","");
				c = "";
				panjang = b.length;
				j = 0;
				for (i = panjang; i > 0; i--){
					j = j + 1;
					if (((j % 3) == 1) && (j != 1)){
						c = b.substr(i-1,1) + "." + c;
					} else {
						c = b.substr(i-1,1) + c;
					}
				}
				if (_minus) c = "-" + c ;
					return c;
				}

			function numbersonly(ini, e){
				if (e.keyCode>=49){
					if(e.keyCode<=57){
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode):b + String.fromCharCode(e.keyCode);
						ini.value = tandaPemisahTitik(b);
						return false;
					} else if(e.keyCode<=105){
						if(e.keyCode>=96){
						//e.keycode = e.keycode - 47;
						a = ini.value.toString().replace(".","");
						b = a.replace(/[^\d]/g,"");
						b = (b=="0")?String.fromCharCode(e.keyCode-48):b + String.fromCharCode(e.keyCode-48);
						ini.value = tandaPemisahTitik(b);
						//alert(e.keycode);
						return false;
					} else {return false;}
				} else {
					return false; 
				}
		}else if (e.keyCode==48){
			a = ini.value.replace(".","") + String.fromCharCode(e.keyCode);
			b = a.replace(/[^\d]/g,"");
			if (parseFloat(b)!=0){
				ini.value = tandaPemisahTitik(b);
				return false;
			} else {
				return false;
		}
	}else if (e.keyCode==95){
		a = ini.value.replace(".","") + String.fromCharCode(e.keyCode-48);
		b = a.replace(/[^\d]/g,"");
		if (parseFloat(b)!=0){
			ini.value = tandaPemisahTitik(b);
			return false;
		} else {
			return false;
		}
	}else if (e.keyCode==8 || e.keycode==46){
		a = ini.value.replace(".","");
		b = a.replace(/[^\d]/g,"");
		b = b.substr(0,b.length -1);
		if (tandaPemisahTitik(b)!=""){
			ini.value = tandaPemisahTitik(b);
		} else {
			ini.value = "";
		}

		return false;
	} else if (e.keyCode==9){
		return true;
	} else if (e.keyCode==17){
		return true;
	} else {
		//alert (e.keyCode);
		return false;
	}

}

    </script>
</body>
</html>
<?php 
} else {
	echo "<script>alert('Session Timeout,silahkan login kembali')
	location.replace('login')
	</script>";
}
?>

